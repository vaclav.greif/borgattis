<?php

namespace Borgattis\Api;


use Borgattis\Cart;
use Borgattis\Core\Rest;
use Borgattis\Managers\RepositoriesManager;
use Borgattis\Models\CartItem;
use Borgattis\Models\Product;
use Borgattis\Settings;

/**
 * Class Rest
 * @package WPProgramator\sonoma
 */
class BoxApi extends Rest
{

  /**
   * @var Settings
   */
  private $settings;
  /**
   * @var Cart
   */
  private $cart;

  /**
   * BoxesApi constructor.
   *
   * @param Settings $settings
   * @param Cart $cart
   */
  public function __construct(Settings $settings, Cart $cart)
  {
    $this->settings = $settings;
    $this->cart     = $cart;
  }

  public function setup()
  {
    add_action('rest_api_init', array($this, 'registerRoutes'));
  }

  /**
   * Register the routes for the objects of the controller.
   */
  public function registerRoutes()
  {
    register_rest_route(
      $this->plugin->getApiManager()->getRestNamespace(),
      'box',
      array(
        array(
          'methods'  => \WP_REST_Server::CREATABLE,
          'callback' => array($this, 'addBox'),
          'permission_callback' => '__return_true',
          'args'     => [
            'size' => [
              'required' => true,
            ],
          ],
        ),
      )
    );
    register_rest_route(
      $this->plugin->getApiManager()->getRestNamespace(),
      'box/(?P<id>[\s\S]+)',
      array(
        array(
          'methods'  => \WP_REST_Server::READABLE,
          'callback' => array($this, 'getBoxDetails'),
          'permission_callback' => '__return_true',
        ),
      )
    );
    register_rest_route(
      $this->plugin->getApiManager()->getRestNamespace(),
      'box/(?P<id>[\s\S]+)',
      array(
        array(
          'methods'  => \WP_REST_Server::DELETABLE,
          'callback' => array($this, 'deleteBox'),
          'permission_callback' => '__return_true',
        ),
      )
    );
    register_rest_route(
      $this->plugin->getApiManager()->getRestNamespace(),
      'box/(?P<id>[\s\S]+)',
      array(
        array(
          'methods'  => \WP_REST_Server::EDITABLE,
          'callback' => array($this, 'changeBoxSize'),
          'permission_callback' => '__return_true',
          'args'     => [
            'size' => [
              'required' => true,
            ],
          ],
        ),
      )
    );
    register_rest_route(
      $this->plugin->getApiManager()->getRestNamespace(),
      'empty-box/(?P<id>[\s\S]+)',
      array(
        array(
          'methods'  => \WP_REST_Server::DELETABLE,
          'callback' => array($this, 'emptyBox'),
          'permission_callback' => '__return_true',
        ),
      )
    );
  }

  /**
   * Add box to cart
   *
   * @param \WP_REST_Request $request Full data about the request.
   *
   * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
   */
  public function addBox($request)
  {
    $helpers = $this->get_plugin()->getHelpers();

    $product_id = $this->settings->getOption('box_product_id');
    $cartItemID = $this->cart->addToCart(
      $product_id,
      1,
      [
        $helpers->prefixString('boxSize') => $request->get_param('size'),
        $helpers->prefixString('boxType') => $request->get_param('boxType'),
        $helpers->prefixString('rand')    => rand(),
      ]
    );

    if (is_wp_error($cartItemID)) {
      return $cartItemID;
    }

    if (!$cartItemID) {
      return new \WP_Error('error-adding-to-cart', __('Error adding the box to cart'), ['status' => 400]);
    }

    return new \WP_REST_Response(
      [
        'id'   => $cartItemID,
        'cart' => $this->plugin->getApiManager()->getSortedCart(),
      ],
      201
    );
  }

  /**
   * Get box details
   *
   * @param \WP_REST_Request $request Full data about the request.
   *
   * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
   */
  public function getBoxDetails($request)
  {
    $box = $this->cart->getBox($request->get_param('id'));

    $i = [];
    foreach ($box['items'] as $item) {
      $i[] = $this->plugin->getApiManager()->getApiCartItem($item);
    }

    return new \WP_REST_Response(
      [
        'box'   => $this->plugin->getApiManager()->getApiBox($box['box']),
        'items' => $i,

      ],
      200
    );
  }

  /**
   * Delete box
   *
   * @param \WP_REST_Request $request Full data about the request.
   *
   * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
   */
  public function deleteBox($request)
  {
    $items = $this->cart->getChildItems($request->get_param('id'));
    foreach ($items as $item) {
      $this->cart->removeCartItem($item->getId());
    }

    $this->cart->removeCartItem($request->get_param('id'));

    return new \WP_REST_Response(
      [
        'status' => true,
        'cart'   => $this->plugin->getApiManager()->getSortedCart(),
      ],
      200
    );
  }

  /**
   * Delete box
   *
   * @param \WP_REST_Request $request Full data about the request.
   *
   * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
   */
  public function emptyBox($request)
  {
    $items = $this->cart->getChildItems($request->get_param('id'));
    foreach ($items as $item) {
      $this->cart->removeCartItem($item->getId());
    }

    return new \WP_REST_Response(
      [
        'status' => true,
        'cart'   => $this->plugin->getApiManager()->getSortedCart(),
      ],
      200
    );
  }


  /**
   * Delete box
   *
   * @param \WP_REST_Request $request Full data about the request.
   *
   * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
   */
  public function changeBoxSize($request)
  {
    $helpers        = $this->get_plugin()->getHelpers();
    $box            = $this->cart->getBox($request->get_param('id'));
    $currentBoxSize = $box['box']->getBoxSize();
    $newBoxSize     = $request->get_param('size');

    $cart = WC()->cart->cart_contents;
    foreach ($cart as $cart_item_id => $cart_item) {
      if ($cart_item_id == $box['box']->getId()) {
        $cart_item[$helpers->prefixString('boxSize')] = $newBoxSize;
        WC()->cart->cart_contents[$cart_item_id]      = $cart_item;
      }
    }

    if ($currentBoxSize > $newBoxSize) {
      // we need to recalculate the quantities
      $currentQuantity = 0;
      foreach ($box['items'] as $item) {
        $currentQuantity += $item->getQuantity();
      }
      $added_items = 0;
      if ($currentQuantity > $newBoxSize) {
        $ratio = $newBoxSize / $currentBoxSize;
        foreach ($box['items'] as $item) {
          if ($added_items >= $newBoxSize) {
            $this->cart->removeCartItem($item->getId());
          } else {
            $quantity = floor($item->getQuantity() * $ratio);
            if (!$quantity) {
              $quantity = 1;
            }
            $added_items += $quantity;
            $this->cart->updateItemQuantity($item->getId(), $quantity);
          }

        }
      }
    }
    WC()->cart->set_session();

    return new \WP_REST_Response(
      [
        'status' => true,
        'cart'   => $this->plugin->getApiManager()->getSortedCart(),
      ],
      200
    );
  }

  /**
   * Check if a given request has access to create items
   *
   * @param \WP_REST_Request $request Full data about the request.
   *
   * @return \WP_Error|bool
   */
  public function create_item_permissions_check($request)
  {
    return true;
  }


  /**
   * Prepare the item for the REST response
   *
   * @param mixed $item WordPress representation of the item.
   * @param \WP_REST_Request $request Request object.
   *
   * @return mixed
   */
  public function prepare_item_for_response($item, $request)
  {
    return array();
  }
}
