<?php

namespace Borgattis\Api;


use Borgattis\Core\Rest;
use Borgattis\Managers\RepositoriesManager;
use Borgattis\Models\Product;

/**
 * Class Rest
 * @package WPProgramator\sonoma
 */
class ProductsApi extends Rest
{
  /**
   * @var RepositoriesManager
   */
  private $repositoriesManager;

  public function __construct(RepositoriesManager $repositoriesManager)
  {
    $this->repositoriesManager = $repositoriesManager;
  }

  public function setup()
  {
    add_action('rest_api_init', array($this, 'registerRoutes'));
  }

  /**
   * Register the routes for the objects of the controller.
   */
  public function registerRoutes()
  {
    register_rest_route(
      $this->plugin->getApiManager()->getRestNamespace(),
      'products',
      array(
        array(
          'methods'  => \WP_REST_Server::READABLE,
          'callback' => array($this, 'getProducts'),
          'permission_callback' => '__return_true',
        ),
      )
    );
  }

  /**
   * Register
   *
   * @param \WP_REST_Request $request Full data about the request.
   *
   * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
   */
  public function getProducts($request)
  {
    $products = $this->repositoriesManager->productRepository->all();

    $result = [];
    foreach ($products as $item) {
      $result[] = $this->getApiProduct($item);
    }

    return new \WP_REST_Response(
      $result,
      200
    );
  }

  public function getApiProduct(Product $product)
  {
    return [
      'id'            => $product->getId(),
      'name'          => $product->getName(),
      'categories'    => $product->getCategories(),
      'categoriesIds' => $product->getCategoriesIds(),
      'details'       => $product->getDetails(),

    ];
  }

  /**
   * Check if a given request has access to create items
   *
   * @param \WP_REST_Request $request Full data about the request.
   *
   * @return \WP_Error|bool
   */
  public function create_item_permissions_check($request)
  {
    return true;
  }


  /**
   * Prepare the item for the REST response
   *
   * @param mixed $item WordPress representation of the item.
   * @param \WP_REST_Request $request Request object.
   *
   * @return mixed
   */
  public function prepare_item_for_response($item, $request)
  {
    return array();
  }
}
