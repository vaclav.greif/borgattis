<?php

namespace Borgattis\Api;


use Borgattis\Core\Rest;
use Borgattis\Helpers;
use Borgattis\Managers\RepositoriesManager;
use Borgattis\Models\Product;

/**
 * Class Rest
 * @package WPProgramator\sonoma
 */
class PostcodeApi extends Rest
{
  /**
   * @var Helpers
   */
  private $helpers;

  public function __construct(Helpers $helpers) {
    $this->helpers = $helpers;
  }
  public function setup()
  {
    add_action('rest_api_init', array($this, 'registerRoutes'));
  }

  /**
   * Register the routes for the objects of the controller.
   */
  public function registerRoutes()
  {
    register_rest_route(
      $this->plugin->getApiManager()->getRestNamespace(),
      'postcode',
      array(
        array(
          'methods'  => \WP_REST_Server::CREATABLE,
          'callback' => array($this, 'validatePostcode'),
          'permission_callback' => '__return_true',
          'args'     => [
            'postcode' => [
              'required' => true,
            ],
          ],
        ),
      )
    );
  }

  /**
   * Register
   *
   * @param \WP_REST_Request $request Full data about the request.
   *
   * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
   */
  public function validatePostcode($request)
  {
    $postcode = $request->get_param('postcode');
    $valid = \WC_Validation::is_postcode( $postcode, WC()->customer->get_billing_country() ?: 'US' );
    if ($valid) {
      $address = $this->helpers->geocodePostcode($postcode);
      if ($address) {
        WC()->customer->set_city($address['locality']);
        WC()->customer->set_shipping_city($address['locality']);
        WC()->customer->set_billing_city($address['locality']);
        WC()->customer->set_postcode($postcode);
        WC()->customer->set_shipping_postcode($postcode);
        WC()->customer->set_billing_postcode($postcode);
        WC()->customer->set_billing_address_1($address['street']);
        WC()->customer->set_billing_state($address['state']);
        WC()->customer->set_shipping_state($address['state']);
        WC()->customer->set_first_name(' ');
        WC()->customer->set_last_name(' ');
      }
    }

    return new \WP_REST_Response(
      array(
        'valid' => \WC_Validation::is_postcode( $request->get_param('postcode'), WC()->customer->get_billing_country() ?: 'US' ),
        'postcode' => $request->get_param('postcode'),
      ),
      200
    );
  }

  public function getApiProduct(Product $product)
  {
    return [
      'id'            => $product->getId(),
      'name'          => $product->getName(),
      'categories'    => $product->getCategories(),
      'categoriesIds' => $product->getCategoriesIds(),
      'details'       => $product->getDetails(),

    ];
  }

  /**
   * Check if a given request has access to create items
   *
   * @param \WP_REST_Request $request Full data about the request.
   *
   * @return \WP_Error|bool
   */
  public function create_item_permissions_check($request)
  {
    return true;
  }


  /**
   * Prepare the item for the REST response
   *
   * @param mixed $item WordPress representation of the item.
   * @param \WP_REST_Request $request Request object.
   *
   * @return mixed
   */
  public function prepare_item_for_response($item, $request)
  {
    return array();
  }
}
