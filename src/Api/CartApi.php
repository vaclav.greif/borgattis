<?php

namespace Borgattis\Api;


use Borgattis\Cart;
use Borgattis\Core\Rest;
use Borgattis\Helpers;
use Borgattis\Models\CartItem;
use ComposePress\Core\Exception\Plugin;
use WP_Error;
use WP_REST_Request;
use WP_REST_Response;
use WP_REST_Server;

/**
 * Class Rest
 * @package WPProgramator\sonoma
 */
class CartApi extends Rest {
  /**
   * @var Cart
   */
  private $cart;
  /**
   * @var Helpers
   */
  private $helpers;

  /**
   * CartApi constructor.
   *
   * @param Cart    $cart
   * @param Helpers $helpers
   */
  public function __construct( Cart $cart, Helpers $helpers ) {
    $this->cart    = $cart;
    $this->helpers = $helpers;
  }

  public function setup() {
    add_action( 'rest_api_init', array( $this, 'registerRoutes' ) );
  }

  /**
   * Register the routes for the objects of the controller.
   */
  public function registerRoutes() {
    register_rest_route(
      $this->plugin->getApiManager()->getRestNamespace(),
      'cart',
      array(
        array(
          'methods'             => WP_REST_Server::CREATABLE,
          'callback'            => array( $this, 'addToCart' ),
          'permission_callback' => '__return_true',
          'args'                => [
            'productId' => [
              'required' => true,
            ],
            'quantity'  => [
              'required' => true,
            ],
          ],
        ),
      )
    );

    register_rest_route(
      $this->plugin->getApiManager()->getRestNamespace(),
      'cart/mtd',
      array(
        array(
          'methods'             => WP_REST_Server::CREATABLE,
          'callback'            => array( $this, 'handleMakeTheirDay' ),
          'permission_callback' => '__return_true',
          'args'                => [
            'product_id' => [
              'required' => true,
            ],
          ],
        ),
      )
    );


    register_rest_route(
      $this->plugin->getApiManager()->getRestNamespace(),
      'cart',
      array(
        array(
          'methods'             => WP_REST_Server::READABLE,
          'callback'            => array( $this, 'getCartContents' ),
          'permission_callback' => '__return_true',
        ),
      )
    );
    register_rest_route(
      $this->plugin->getApiManager()->getRestNamespace(),
      'cart/sorted',
      array(
        array(
          'methods'             => WP_REST_Server::READABLE,
          'callback'            => array( $this, 'getSortedCart' ),
          'permission_callback' => '__return_true',
        ),
      )
    );
    register_rest_route(
      $this->plugin->getApiManager()->getRestNamespace(),
      'checkout/validate/customer',
      array(
        array(
          'methods'             => WP_REST_Server::CREATABLE,
          'callback'            => array( $this, 'validateCustomer' ),
          'permission_callback' => '__return_true',
        ),
      )
    );
    register_rest_route(
      $this->plugin->getApiManager()->getRestNamespace(),
      'cart/(?P<id>[\s\S]+)',
      array(
        array(
          'methods'             => WP_REST_Server::CREATABLE,
          'callback'            => array( $this, 'updateQuantity' ),
          'permission_callback' => '__return_true',
          'args'                => [
            'quantity' => [
              'required' => true,
            ],
          ],
        ),
      )
    );
  }

  /**
   * Add product to cart
   *
   * @param WP_REST_Request $request Full data about the request.
   *
   * @return WP_Error|WP_REST_Request|WP_REST_Response | bool
   */
  public function addToCart( $request ) {
    $data  = [];
    $boxId = $request->get_param( 'parentId' );
    if ( $boxId ) {
      $data[ $this->helpers->prefixString( 'parentId' ) ] = $boxId;

      $box      = $this->cart->getBox( $boxId );
      $quantity = 0;
      foreach ( $box['items'] as $item ) {
        $quantity += $item->getQuantity();
      }

      if ( $box['box']->getBoxSize() <= $quantity && $request->get_param( 'quantity' ) > 0 ) {
        return new WP_REST_Response(
          [
            'id'    => $boxId,
            'cart'  => $this->plugin->getApiManager()->getSortedCart(),
            'error' => __( 'The box is full, please add a new box', 'borgattis' ),
          ],
          400
        );
      }
    }

    $cartItemId = $this->cart->addToCart( $request->get_param( 'productId' ), $request->get_param( 'quantity' ), $data );
    if ( is_wp_error( $cartItemId ) ) {
      return $cartItemId;
    }

    if ( ! $cartItemId ) {
      return new WP_REST_Response(
        [
          'id'    => $boxId,
          'cart'  => $this->plugin->getApiManager()->getSortedCart(),
          'error' => __( 'Error adding the item to cart', 'borgattis' ),
        ],
        400
      );
    }

    return new WP_REST_Response(
      [
        'id'   => $cartItemId,
        'cart' => $this->plugin->getApiManager()->getSortedCart(),
      ],
      201
    );
  }

  /**
   * Get cart contents
   *
   * @param WP_REST_Request $request Full data about the request.
   *
   * @return WP_Error|WP_REST_Request|WP_REST_Response | bool
   */
  public function getCartContents( $request ) {
    $cart   = $this->cart->getCart();
    $result = [];
    foreach ( $cart as $item ) {
      $result[] = $this->plugin->getApiManager()->getApiCartItem( $item );
    }

    return new WP_REST_Response(
      $result,
      200
    );
  }

  /**
   * Handle make their day
   *
   * @param WP_REST_Request $request Full data about the request.
   *
   * @return WP_Error|WP_REST_Request|WP_REST_Response | bool
   */
  public function handleMakeTheirDay( WP_REST_Request $request ) {
    foreach ( $request->get_params() as $key => $value ) {
      if ( strpos( $key, 'billing' ) !== false ) {
        $method_name = 'set_' . $key;
        WC()->customer->$method_name( $value );
      }
    }
    WC()->session->set( 'borgattis_is_gift', true );
    $message = $request->get_param( 'gift_message' );
    WC()->session->set( 'borgattis_gift_first_name', $request->get_param( 'billing_first_name' ) );
    WC()->session->set( 'borgattis_gift_last_name', $request->get_param( 'billing_last_name' ) );
    if ( ! empty( $message ) ) {
      WC()->session->set( 'borgattis_gift_message', $message );
    }
    $variation_data = [];
    $variation_id   = $request->get_param( 'variation_id' ) ?: 0;
    if ( ! empty( $request->get_param( 'variation_data' ) ) ) {
      $variation_data = $request->get_param( 'variation_data' );
    }

    $item = $this->cart->addToCart( $request->get_param( 'product_id' ), 1, [], $variation_id, $variation_data );

    return new WP_REST_Response(
      [
        'status' => 'ok',
      ],
      200
    );
  }


  /**
   * Get sorted cart contents
   *
   * @param WP_REST_Request $request Full data about the request.
   *
   * @return WP_Error|WP_REST_Request|WP_REST_Response | bool
   */
  public function getSortedCart( $request ) {
    return new WP_REST_Response(
      $this->plugin->getApiManager()->getSortedCart(),
      200
    );
  }

  /**
   * Get sorted cart contents
   *
   * @param WP_REST_Request $request Full data about the request.
   *
   * @return WP_Error|WP_REST_Request|WP_REST_Response | bool
   * @throws Plugin
   */
  public function updateQuantity( $request ) {
    $item_id = $request->get_param( 'id' );
    $item    = $this->cart->getCartItem( $item_id );
    $box     = $this->cart->getBox( $item->getParentId() );


    $quantity = 0;
    foreach ( $box['items'] as $item ) {
      if ( $item->getId() == $item_id ) {
        $quantity += $request->get_param( 'quantity' );
      } else {
        $quantity += $item->getQuantity();
      }
    }

    if ( $box['box']->getBoxSize() < $quantity && $request->get_param( 'quantity' ) > 0 ) {
      return new WP_REST_Response(
        [
          'cart'  => $this->plugin->getApiManager()->getSortedCart(),
          'error' => __( 'The box is full, please add a new box', 'borgattis' ),
        ],
        400
      );
    }

    $this->cart->updateItemQuantity( $request->get_param( 'id' ), $request->get_param( 'quantity' ) );

    return new WP_REST_Response(
      [
        'status' => 'ok',
        'cart'   => $this->plugin->getApiManager()->getSortedCart(),
      ],
      200
    );
  }

  /**
   * Get sorted cart contents
   *
   * @param WP_REST_Request $request Full data about the request.
   *
   * @return void
   */
  public function validateCustomer( $request ) {
    $fields = WC()->checkout()->get_checkout_fields();
    $errors = [];
    unset( $fields['shipping'] );

    foreach ( $fields as $fieldset ) {
      foreach ( $fieldset as $key => $field ) {
        if ( $key == 'account_password' ) {
          continue;
        }
        if ( isset( $field['required'] ) && $field['required'] && ! $request->get_param( $key ) ) {
          $errors[] = [
            'field' => $key,
            'error' => sprintf( 'Please enter %s', $field['label'] ),
          ];
        }
        if ( $key === 'billing_email' && ! \WC_Validation::is_email( $request->get_param( $key ) ) ) {
          $errors[] = [
            'field' => $key,
            'error' => 'Please enter valid email',
          ];
        }
        if ( ( $key === 'billing_postcode' || $key === 'shipping_postcode' ) && ! \WC_Validation::is_postcode(
            $request->get_param( $key ),
            $request->get_param( 'billing_country' )
          ) ) {
          $errors[] = [
            'field' => $key,
            'error' => 'Please enter valid postcode',
          ];
        }
        if ( $key === 'billing_billing_phone' && \WC_Validation::is_phone( $request->get_param( 'billing_phone' ) ) ) {
          $errors[] = [
            'field' => $key,
            'error' => 'Please enter valid phone',
          ];
        }
      }
    }

    return new \WP_REST_Response(
      array(
        'status'           => 'success',
        'errors'           => $errors,
        'formatted_errors' => implode(
          "<br/>",
          array_map(
            function ( $item ) {
              return $item['error'];
            },
            $errors
          )
        ),
      ),
      200
    );
  }

  /**
   * Check if a given request has access to create items
   *
   * @param WP_REST_Request $request Full data about the request.
   *
   * @return WP_Error|bool
   */
  public
  function create_item_permissions_check(
    $request
  ) {
    return true;
  }

  /**
   * Prepare the item for the REST response
   *
   * @param mixed           $item WordPress representation of the item.
   * @param WP_REST_Request $request Request object.
   *
   * @return mixed
   */
  public
  function prepare_item_for_response(
    $item,
    $request
  ) {
    return array();
  }
}
