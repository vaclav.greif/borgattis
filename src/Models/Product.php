<?php

namespace Borgattis\Models;

use Borgattis\Core\Component;

class Product extends Component
{

  private $id;
  private $product;
  private $name = '';
  private $imageUrl = '';
  private $imageUrlLarge = '';
  private $popupImageUrl = '';
  private $imageThumb = [];
  private $imageLarge = [];
  private $popupImage = [];
  private $customFields = [
    'pieces_per_box_no',
    'pairing',
    'servings_no',
    'ingredients',
    'how_to_cook',
    'is_frozen_product',
    'mtd_title'
  ];
  private $categories = [];
  private $categoriesIds = [];
  private $details = [];
  private $price;
  private $stockStatus;

  public function __construct($id)
  {
    $this->id = $id;
  }

  public function setup()
  {
    if (is_numeric($this->id)) {
      $this->product = wc_get_product($this->id);
    } elseif (is_a($this->id, '\WC_Product')) {
      $this->id      = $this->id->get_id();
      $this->product = $this->id;
    }
    if ($this->id) {
      $this->name = $this->product->get_name();

      foreach ($this->customFields as $customField) {
        $this->details[$customField] = nl2br($this->product->get_meta($this->plugin->getPrefix() . $customField, true));
      }

      $this->categoriesIds = $this->product->get_category_ids();

      foreach ($this->categoriesIds as $id) {
        $this->categories[] = get_term($id, 'product_cat');
      }

      $image_thumb  = wp_get_attachment_image_src($this->product->get_image_id(), 'woocommerce_single');
      $image_large  = wp_get_attachment_image_src($this->product->get_image_id(), 'full');
      $popupImageId = $this->product->get_meta(
        $this->plugin->getPrefix() . 'popup_image_id',
        true
      ) ?: $this->product->get_image_id();
      $popupImage   = wp_get_attachment_image_src($popupImageId, 'full');

      if (empty($image_thumb)) {
        $image_thumb = wp_get_attachment_image_src(get_option('woocommerce_placeholder_image', 0));
      }
      if (empty($popupImage)) {
        $popupImage = wp_get_attachment_image_src(get_option('woocommerce_placeholder_image', 0));
      }

      if ($image_thumb) {
        list($url, $width, $height) = $image_thumb;
        $this->imageUrl   = $url;
        $this->imageThumb = [
          'url'    => $url,
          'width'  => $width,
          'height' => $height,
        ];
        list($url, $width, $height) = $image_large;
        $this->imageUrlLarge = $url;
        $this->imageLarge    = [
          'url'    => $url,
          'width'  => $width,
          'height' => $height,
        ];
      }

      if ($popupImage) {
        list($url, $width, $height) = $popupImage;
        $this->popupImageUrl = $url;
        $this->popupImage    = [
          'url'    => $url,
          'width'  => $width,
          'height' => $height,
        ];
      }

      $this->price       = $this->product->get_price();
      $this->stockStatus = $this->product->get_stock_status();
    }
  }

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @return \WC_Product
   */
  public function getProduct()
  {
    return $this->product;
  }


  /**
   * @return array
   */
  public function getCategoriesIds(): array
  {
    return $this->categoriesIds;
  }

  /**
   * @return array
   */
  public function getCategories(): array
  {
    return $this->categories;
  }

  /**
   * @return array
   */
  public function getDetails(): array
  {
    return $this->details;
  }

  /**
   * @return string
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * @return string
   */
  public function getImageUrl(): string
  {
    return $this->imageUrl;
  }

  /**
   * @return string
   */
  public function getImageUrlLarge(): string
  {
    return $this->imageUrl;
  }

  /**
   * @return string
   */
  public function getImageThumb(): array
  {
    return $this->imageThumb;
  }

  /**
   * @return string
   */
  public function getImageLarge(): array
  {
    return $this->imageLarge;
  }

  /**
   * @return mixed
   */
  public function getPrice()
  {
    return $this->price;
  }

  /**
   * @return mixed
   */
  public function getStockStatus()
  {
    return $this->stockStatus;
  }

  /**
   * @return array
   */
  public function getPopupImage(): array
  {
    return $this->popupImage;
  }

}
