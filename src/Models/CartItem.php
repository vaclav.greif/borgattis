<?php

namespace Borgattis\Models;

use Borgattis\Core\Component;
use Borgattis\Plugin;
use Borgattis\Repositories\ProductRepository;

/**
 * Class CartItem
 * @package Borgattis\Models
 * @property Plugin $plugin
 */
class CartItem extends Component
{

  private $id;
  private $cartItem;
  private $name;
  private $type;
  private $boxSize;
  private $boxType;
  private $quantity;
  private $parentId = 0;
  private $price;
  private $productId;
  private $weight;
  private $imageUrl;

  public function __construct($id)
  {
    $this->id = $id;
  }

  public function setup()
  {
    foreach (WC()->cart->get_cart() as $key => $item) {
      if ($key == $this->id) {
        $this->cartItem = $item;
      }
    }

    $this->quantity  = $this->cartItem['quantity'];
    $this->name      = $this->cartItem['data']->get_name();


    $this->parentId  = isset($this->cartItem[$this->plugin->helpers->prefixString('parentId')]) ? $this->cartItem[$this->plugin->helpers->prefixString('parentId')] : 0;
    $this->type      = isset($this->cartItem[$this->plugin->helpers->prefixString('boxSize')]) ? 'box' : 'cartItem';
    $this->boxSize   = isset($this->cartItem[$this->plugin->helpers->prefixString('boxSize')]) ? $this->cartItem[$this->plugin->helpers->prefixString('boxSize')] : '';
    $this->boxType   = isset($this->cartItem[$this->plugin->helpers->prefixString('boxType')]) ? $this->cartItem[$this->plugin->helpers->prefixString('boxType')] : '';

    $this->price     = $this->cartItem['data']->get_price();
    $this->productId = $this->cartItem['data']->get_id();
    $this->weight = $this->cartItem['data']->get_weight();
    /** @var Product $product */
    $product = $this->plugin->getRepositoriesManager()->ProductRepository->get($this->productId);
    $this->imageUrl = $product->getImageUrl();
  }

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }


  /**
   * @return mixed
   */
  public function getCartItem()
  {
    return $this->cartItem;
  }

  /**
   * @return mixed
   */
  public function getQuantity()
  {
    return $this->quantity;
  }

  /**
   * @return mixed
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @return mixed
   */
  public function getParentId()
  {
    return $this->parentId;
  }

  /**
   * @return mixed
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * @return mixed
   */
  public function getBoxSize()
  {
    return $this->boxSize;
  }

  /**
   * @return mixed
   */
  public function getPrice()
  {
    return $this->price;
  }

  /**
   * @return mixed
   */
  public function getBoxType()
  {
    return $this->boxType;
  }

  /**
   * @return mixed
   */
  public function getProductId()
  {
    return $this->productId;
  }

  /**
   * @return mixed
   */
  public function getWeight()
  {
    return $this->weight;
  }

  /**
   * @return mixed
   */
  public function getImageUrl()
  {
    return $this->imageUrl;
  }
}
