<?php

namespace Borgattis;


use Borgattis\Core\Component;
use Borgattis\Models\Product;

/**
 * @property Plugin $plugin
 */
class Frontend extends Component {
  public function setup() {
    add_action( 'wp_enqueue_scripts', [ $this, 'frontendScripts' ] );

    add_shortcode(
      'cart-app',
      function () {
        return '<div id="cart-app"></div>';
      }
    );
    add_action( 'wp_footer', [ $this, 'addPopup' ] );
    add_shortcode( 'borgattis_make_their_day', [ $this, 'render_make_their_day' ] );

    //do_action( 'woocommerce_before_checkout_form', [ $this, 'render_checkout_app' ] );
    add_filter( 'body_class', [ $this, 'add_body_classes' ] );
  }

  public function render_checkout_app() {
    if ( WC()->session->get( 'borgattis_is_gift' ) ) {
      return;
    }
    echo '<div id="checkout-app" class="borgattis-wizard"></div>';
  }

  public function frontendScripts() {
    wp_enqueue_style(
      'font-bebas-neue',
      'https://fonts.googleapis.com/css?family=Bebas+Neue:400%7COswald:300&display=swap'
    );

    wp_enqueue_style( 'borgattis-make-their-day', $this->plugin->get_asset_url( 'assets/css/make-their-day.css' ) );
    wp_enqueue_script( 'borgattis-make-their-day', $this->plugin->get_asset_url( 'assets/js/make-their-day.js' ), [], null, true );

    wp_localize_script(
      'borgattis-make-their-day',
      'mtd',
      [
        'restUrl'     => $this->plugin->getApiManager()->getRestUrl(),
        'checkoutUrl' => wc_get_checkout_url(),
      ]
    );

    wp_enqueue_style( 'borgattis-flickity', 'https://unpkg.com/flickity@2/dist/flickity.min.css' );
    wp_enqueue_script( 'borgattis-flickity', 'https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js', [], null, true );


//    if ( is_checkout() ) {
//      $checkout_app = require $this->plugin->get_asset_path( 'build/checkout-navigation.asset.php' );
//      wp_enqueue_script(
//        'checkout-app',
//        $this->plugin->get_asset_url( 'build/checkout-navigation.js' ),
//        $checkout_app['dependencies'],
//        $checkout_app['version'],
//        true
//      );
//
//      wp_enqueue_style(
//        'checkout-app',
//        $this->plugin->get_asset_url( 'build/plugin.css' )
//      );
//
//      wp_localize_script(
//        'checkout-app',
//        'borgattisCheckoutApp',
//        [
//          'wizard_page' => get_permalink( $this->plugin->settings->getOption( 'wizard_page_id' ) ),
//          'publicPath'  => $this->plugin->get_asset_url( 'build' ) . '/',
//        ]
//      );
//    }

    if ( get_the_ID() == $this->plugin->settings->getOption( 'wizard_page_id' ) ) {
      $cart_app = require $this->plugin->get_asset_path( 'build/cartApp.asset.php' );

      wp_enqueue_script(
        'cart-app',
        $this->plugin->get_asset_url( 'build/cartApp.js' ),
        $cart_app['dependencies'],
        $cart_app['version'],
        true
      );

      wp_enqueue_style(
        'cart-app',
        $this->plugin->get_asset_url( 'build/plugin.css' ),
      );

      $sizes = array_map(
        function ( $box ) {
          $box['size'] = intval( $box['size'] );

          return $box;
        },
        $this->plugin->settings->getOption( 'box_sizes' )
      );

      $fettuccineSizes = array_map(
        function ( $box ) {
          $box['size'] = intval( $box['size'] );

          return $box;
        },
        $this->plugin->settings->getOption( 'box_sizes_fettuccine' )
      );


      $selectedProducts = $this->plugin->getRepositoriesManager()->productRepository->all()->filter(
        function ( $product ) {
          return in_array( $product->getId(), $this->plugin->settings->getOption( 'products_ids' ) );
        }
      );

      $products   = [];
      $categories = [];

      foreach ( $selectedProducts as $p ) {
        $product                 = $this->plugin->getApiManager()->getApiProduct( $p );
        $products[ $p->getId() ] = $product;

        foreach ( $p->getCategories() as $category ) {
          if ( empty( $categories[ $category->term_id ] ) ) {
            $categories[ $category->term_id ] = $category;
          }
        }
      }


      $categories_order = $this->plugin->settings->getOption( 'categories_order' );
      if ( $categories_order ) {
        $sorted_categories = [];
        foreach ( explode( ',', $categories_order ) as $cat_id ) {
          if ( isset( $categories[ $cat_id ] ) ) {
            $sorted_categories[] = $categories[ $cat_id ];
          }
        }
        $categories = $sorted_categories;
      }


      $currentBox = $this->plugin->getCart()->getCart()->filter(
        function ( $item ) {
          return $item->getType() == 'box';
        }
      )->last();

      if ( $currentBox ) {
        $currentBox = $currentBox->getId();
      }

      $bestsellersIds = $this->plugin->settings->getOption( 'bestsellers_ids' );

      $bestsellers = array_filter(
        $products,
        function ( $product ) use ( $bestsellersIds ) {
          return in_array( $product['id'], $bestsellersIds );
        }
      );

      wp_localize_script(
        'cart-app',
        'cartApp',
        [
          'publicPath' => $this->plugin->get_asset_url( 'build' ) . '/',
          'restUrl'    => $this->plugin->getApiManager()->getRestUrl(),
          'introImage'    => wp_get_attachment_url($this->plugin->settings->getOption('intro_image_id')),
          'state'      => [
            'cart'       => [
              'boxes'      => $this->plugin->getApiManager()->getSortedCart(),
              'isShown'    => false,
              'currentBox' => $currentBox,
            ],
            'boxes'      => [
              'standard'   => $sizes,
              'fettuccine' => $fettuccineSizes,
            ],
            'products'   => [
              'all'         => [
                'entities' => $products,
                'ids'      => array_keys( $products ),
              ],
              'bestSellers' => [
                'entities' => $bestsellers,
                'ids'      => array_keys( $bestsellers ),
              ],
            ],
            'categories' => [
              'entities' => $categories,
              'ids'      => array_keys( $categories ),
            ],
            'wizard'     => [
              'step' => 1,
            ],
          ],
        ],
      );
    }
//    if ( is_checkout() ) {
//      wp_enqueue_style( 'borgattis-checkout', $this->plugin->get_asset_url( 'assets/css/checkout.css' ) );
//      wp_enqueue_script( 'borgattis-checkout', $this->plugin->get_asset_url( 'assets/js/checkout.js' ) );
//      wp_localize_script(
//        'borgattis-checkout',
//        'borgattisCheckoutVars',
//        [
//          'restUrl' => $this->plugin->getApiManager()->getRestUrl(),
//        ],
//      );
//    }
  }


  public function addPopup() { ?>
    <div id="borgattis-popup"></div>
    <?php
  }

  public function render_make_their_day() {
    if ( is_admin() ) {
      return 'Make their day';
    }

    $args = [
      'category' => array( 'gifts' ),
    ];
    add_filter( 'woocommerce_product_single_add_to_cart_text', function () {
      return __( 'Send gift', 'woocommerce' );
    } );

    $products = $this->plugin->getRepositoriesManager()->productRepository->find( $args );
    //die(var_dump($products->count()));;
    ob_start(); ?>
    <div class="mtd">
      <div class="mtd__products">
        <?php
        foreach ( $products as $item ) {
          /** @var Product $product */
          ?>
          <div class="mtd__product">
            <h2><?php
              echo $item->getName(); ?></h2>
            <div class="mtd__details row wpb_row">
              <div class="wpb_column columns medium-6 small-12">
                <?php
                $gallery = $item->getProduct()->get_gallery_image_ids();
                ?>
                <div class="mtd__gallery <?php echo !empty($gallery) ? 'mtd__gallery--thumbs' : '';?>">
                  <?php

                  if ( ! empty( $gallery ) ) { ?>
                    <div class="carousel carousel-nav"
                         data-flickity='{
                      "asNavFor": ".carousel-main-<?php echo $item->getId();?>",
                      "draggable": true,
                      "percentPosition": false,
                      "groupCells": "100%",
                      "pageDots": false,
                      "prevNextButtons": false
                    }'>
                      <div class="carousel-cell"><?php echo wp_get_attachment_image( $item->getProduct()->get_image_id() ); ?></div>

                      <?php foreach ( $gallery as $thumb ) { ?>
                        <div class="carousel-cell"><?php echo wp_get_attachment_image( $thumb ); ?></div>
                      <?php } ?>
                    </div>
                    <?php
                  } ?>
                  <div class="carousel-main carousel-main-<?php echo $item->getId();?>" data-flickity='{"pageDots": false,"contain": true, "adaptiveHeight": true}'>
                    <div class="carousel-cell"><?php echo $item->getProduct()->get_image(); ?></div>
                    <?php if ( ! empty( $gallery ) ) {
                      foreach ( $gallery as $thumb ) { ?>
                        <div class="carousel-cell"><?php echo wp_get_attachment_image( $thumb, 'full' ); ?></div>
                        <?php
                      }
                    } ?>
                  </div>

                </div>

              </div>
              <div class="wpb_column columns medium-6 small-12">
                <div class="mtd__product-header">
                  <?php
                  if ( ! empty( $item->getDetails()['mtd_title'] ) ) { ?>
                    <h3><?php
                      echo $item->getDetails()['mtd_title'] ?></h3>
                    <?php
                  } ?>
                  <div>
                    <?php
                    echo wc_price( $item->getPrice() ); ?>
                  </div>
                </div>
                <?php
                echo apply_filters( 'the_content', $item->getProduct()->get_description() ); ?>
                <?php

                /** @var \WC_Product_Variable $wc_product */
                $wc_product         = $item->getProduct();
                $GLOBALS['product'] = $wc_product;
                if ( $item->getProduct()->is_type( 'variable' ) ) {
                  wc_get_template(
                    'single-product/add-to-cart/variable.php',
                    array(
                      'available_variations' => $wc_product->get_available_variations(),
                      'attributes'           => $wc_product->get_variation_attributes(),
                      'selected_attributes'  => $wc_product->get_default_attributes(),
                    )
                  );
                } else {
                  wc_get_template( 'single-product/add-to-cart/simple.php' );
                }
                ?>
              </div>
            </div>
          </div>
          <?php
        } ?>
      </div>

      <form action="" class="mtd__form">
        <div class="row wpb_row">
          <div class="wpb_column columns medium-6 small-12">
            <?php
            woocommerce_form_field(
              'billing_first_name',
              array(
                'type'     => 'text',
                'label'    => __( 'Their First Name' ),
                'required' => true,
              )
            ); ?>
          </div>
          <div class="wpb_column columns medium-6 small-12">
            <?php
            woocommerce_form_field(
              'billing_last_name',
              array(
                'type'     => 'text',
                'label'    => __( 'Their Last Name' ),
                'required' => true,
              )
            ); ?>
          </div>

          <div class="wpb_column columns medium-12">
            <?php
            woocommerce_form_field(
              'billing_address_1',
              array(
                'type'     => 'text',
                'label'    => __( 'Street Address' ),
                'required' => true,
              )
            ); ?>
            <?php
            woocommerce_form_field(
              'billing_address_2',
              array(
                'type'     => 'text',
                'label'    => __( 'Apartment, suite, unit etc' ),
                'required' => true,
              )
            ); ?>
            <?php
            woocommerce_form_field(
              'billing_city',
              array(
                'type'     => 'text',
                'label'    => __( 'City' ),
                'required' => true,
              )
            ); ?>
            <?php
            woocommerce_form_field(
              'billing_state',
              array(
                'type'        => 'state',
                'label'       => __( 'State' ),
                'placeholder' => __( 'Select a State' ),
                'required'    => true,
              )
            ); ?>
            <?php
            woocommerce_form_field(
              'billing_postcode',
              array(
                'type'     => 'text',
                'label'    => __( 'ZIP' ),
                'required' => true,
              )
            ); ?>
            <?php
            woocommerce_form_field(
              'gift_message',
              array(
                'type'              => 'textarea',
                'label'             => __( 'Gift message' ),
                'custom_attributes' => [
                  'cols' => 20,
                ],
              )
            ); ?>
            <input type="submit" value="Continue to billing"/>
          </div>
        </div>
      </form>
    </div>
    <?php

    return ob_get_clean();
    ?>

    <?php
  }

  public function add_body_classes( $classes ) {
    if ( get_the_ID() == $this->plugin->settings->getOption( 'wizard_page_id' ) ) {
      $classes[] = 'borgattis-wizard-page';
    }

    return $classes;
  }
}
