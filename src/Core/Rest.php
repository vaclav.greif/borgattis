<?php

namespace Borgattis\Core;

use Borgattis\Managers\ApiManager;
use ComposePress\Core\Traits\Component_0_9_0_0;

/**
 * Class Component
 * @package Borgattis\Core
 * * @property \Borgattis\Plugin $plugin
 */
abstract class Rest extends \WP_REST_Controller
{
  use Component_0_9_0_0;
}
