<?php

namespace Borgattis;

use Borgattis\Core\Component;
use Borgattis\Managers\RepositoriesManager;

/**
 * Class Settings
 * @package WPProgramator\DFCOMPETITIONS
 */
class Settings extends Component
{

  public $options = [];
  /**
   * Options Page title
   * @var string
   */
  protected $title = '';
  /**
   * Options Page hook
   * @var string
   */
  protected $options_page = '';
  /**
   * Option key, and option page slug
   * @var string
   */
  private $key = 'borgattis_options';
  /**
   * Options page metabox id
   * @var string
   */
  private $metabox_id = 'borgattis_options_metabox';
  /**
   * @var RepositoriesManager
   */
  private $repositoriesManager;

  /**
   * Constructor
   * @since 0.1.0
   */
  public function __construct(RepositoriesManager $repositoriesManager)
  {
    // Set our title
    $this->title = __('Borgattis Settings', 'borgattis');
    $this->hooks();
    $this->repositoriesManager = $repositoriesManager;
  }

  /**
   * Initiate our hooks
   * @since 0.1.0
   */
  public function hooks()
  {
    add_action('admin_init', array($this, 'init'));
    add_action('admin_menu', array($this, 'add_options_page'));
    add_action('cmb2_admin_init', array($this, 'add_options_page_metabox'));
  }


  /**
   * Register our setting to WP
   * @since  0.1.0
   */
  public function init()
  {
    register_setting($this->key, $this->key);
  }

  /**
   * Add menu options page
   * @since 0.1.0
   */
  public function add_options_page()
  {
    $this->options_page = add_menu_page(
      $this->title,
      $this->title,
      'manage_options',
      $this->key,
      array($this, 'admin_page_display')
    );
    // Include CMB CSS in the head to avoid FOUC
    add_action("admin_print_styles-{$this->options_page}", array('CMB2_hookup', 'enqueue_cmb_css'));
  }

  /**
   * Admin page markup. Mostly handled by CMB2
   * @since  0.1.0
   */
  public function admin_page_display()
  {
    ?>
    <div class="wrap cmb2-options-page <?php echo $this->key; ?>">
      <h2><?php echo esc_html(get_admin_page_title()); ?></h2>
      <?php cmb2_metabox_form($this->metabox_id, $this->key); ?>
    </div>
    <?php
  }

  /**
   * Add the options metabox to the array of metaboxes
   * @since  0.1.0
   */
  public function add_options_page_metabox()
  {
    // hook in our save notices
    add_action("cmb2_save_options-page_fields_{$this->metabox_id}", array($this, 'settings_notices'), 10, 2);

    $cmb = new_cmb2_box(
      [
        'id'         => $this->metabox_id,
        'hookup'     => false,
        'cmb_styles' => true,
        'show_on'    => [
          // These are important, don't remove
          'key'   => 'options-page',
          'value' => [$this->key],
        ],
      ]
    );

    $products = $this->repositoriesManager->productRepository->all();
    $options  = [];
    foreach ($products as $item) {
      $options[$item->getId()] = $item->getName();
    }

    $cmb->add_field(
      array(
        'name'    => __('Virtual Box Product', 'borgattis'),
        'id'      => 'box_product_id',
        'type'    => 'select',
        'options' => $options,
      )
    );

    $pages = get_posts(
      [
        'posts_per_page' => -1,
        'post_type'      => 'page',
      ]
    );

    $pages_options = [];
    foreach ($pages as $page) {
      $pages_options[$page->ID] = $page->post_title;
    };

    $cmb->add_field(
      array(
        'name'    => __('Wizard page', 'borgattis'),
        'id'      => 'wizard_page_id',
        'type'    => 'select',
        'desc'    => __('Select the Wizard page', 'borgattis'),
        'options' => $pages_options,
      )
    );


    $box_sizes = $cmb->add_field(
      array(
        'id'      => 'box_sizes',
        'type'    => 'group',
        // 'repeatable'  => false, // use false if you want non-repeatable group
        'options' => array(
          'group_title'   => __('Box Size {#}', 'cmb2'), // since version 1.1.4, {#} gets replaced by row number
          'add_button'    => __('Add Another Box Size', 'cmb2'),
          'remove_button' => __('Remove Box Size', 'cmb2'),
          'sortable'      => true,
        ),
      )
    );

    $cmb->add_group_field(
      $box_sizes,
      array(
        'name' => __('Size', 'borgattis'),
        'id'   => 'size',
        'type' => 'text',
      )
    );
    $cmb->add_group_field(
      $box_sizes,
      array(
        'name' => __('Name', 'borgattis'),
        'id'   => 'name',
        'type' => 'text',
      )
    );
    $cmb->add_group_field(
      $box_sizes,
      array(
        'name' => __('Extra weight if box contains frozen products', 'borgattis'),
        'id'   => 'extra_weight',
        'type' => 'text',
      )
    );

    $cmb->add_group_field(
      $box_sizes,
      array(
        'name' => __('Length', 'borgattis'),
        'id'   => 'length',
        'type' => 'text',
      )
    );


    $cmb->add_group_field(
      $box_sizes,
      array(
        'name' => __('Width', 'borgattis'),
        'id'   => 'width',
        'type' => 'text',
      )
    );

    $cmb->add_group_field(
      $box_sizes,
      array(
        'name' => __('Height', 'borgattis'),
        'id'   => 'height',
        'type' => 'text',
      )
    );

    $box_sizes = $cmb->add_field(
      array(
        'id'      => 'box_sizes_fettuccine',
        'type'    => 'group',
        // 'repeatable'  => false, // use false if you want non-repeatable group
        'options' => array(
          'group_title'   => __('Box Size Fettuccine {#}', 'cmb2'),
          // since version 1.1.4, {#} gets replaced by row number
          'add_button'    => __('Add Another Box Size', 'cmb2'),
          'remove_button' => __('Remove Box Size', 'cmb2'),
          'sortable'      => true,
        ),
      )
    );

    $cmb->add_group_field(
      $box_sizes,
      array(
        'name' => __('Size', 'borgattis'),
        'id'   => 'size',
        'type' => 'text',
      )
    );
    $cmb->add_group_field(
      $box_sizes,
      array(
        'name' => __('Name', 'borgattis'),
        'id'   => 'name',
        'type' => 'text',
      )
    );

    $cmb->add_group_field(
      $box_sizes,
      array(
        'name' => __('Extra weight if box contains frozen products', 'borgattis'),
        'id'   => 'extra_weight',
        'type' => 'text',
      )
    );
    $cmb->add_group_field(
      $box_sizes,
      array(
        'name' => __('Length', 'borgattis'),
        'id'   => 'length',
        'type' => 'text',
      )
    );


    $cmb->add_group_field(
      $box_sizes,
      array(
        'name' => __('Width', 'borgattis'),
        'id'   => 'width',
        'type' => 'text',
      )
    );

    $cmb->add_group_field(
      $box_sizes,
      array(
        'name' => __('Height', 'borgattis'),
        'id'   => 'height',
        'type' => 'text',
      )
    );

    $cmb->add_field(
      array(
        'name'    => __('Products', 'borgattis'),
        'id'      => 'products_ids',
        'type'    => 'multicheck',
        'options' => $options,
      )
    );

    $cmb->add_field(
      array(
        'name'       => __('Bestsellers', 'borgattis'),
        'id'         => 'bestsellers_ids',
        'type'       => 'select',
        'options'    => $options,
        'repeatable' => true,
      )
    );

    $cmb->add_field(
      array(
        'name' => __('Categories order', 'borgattis'),
        'id'   => 'categories_order',
        'type' => 'text',
        'desc' => __('Enter categories IDs separated by comma', 'borgattis'),
      )
    );


    $cmb->add_field(
      array(
        'name' => __('Checkout box heading', 'borgattis'),
        'id'   => 'checkout_box_heading',
        'type' => 'text',
      )
    );

    $cmb->add_field(
      array(
        'name' => __('Checkout box heading', 'borgattis'),
        'id'   => 'checkout_box_heading',
        'type' => 'text',
      )
    );

    $cmb->add_field(
      array(
        'name' => __('Checkout box content', 'borgattis'),
        'id'   => 'checkout_box_content',
        'type' => 'wysiwyg',
      )
    );

    $cmb->add_field(
      array(
        'name' => __('Google Maps API Key', 'borgattis'),
        'id'   => 'google_maps_api_key',
        'type' => 'text',
      )
    );

    $cmb->add_field(
      array(
        'name'    => __('Intro Image', 'borgattis'),
        'id'      => 'intro_image',
        'type'    => 'file',
      )
    );

  }

  public function field_upload_results()
  { ?>
    <input type="file" name="superpes-results"/>
  <?php }

  public function field_upload_results_sanitize($override_value, $value)
  {
    return $override_value;
  }

  /**
   * Register settings notices for display
   *
   * @param int $object_id Option key
   * @param array $updated Array of updated fields
   *
   * @return void
   * @since  0.1.0
   */
  public
  function settings_notices(
    $object_id,
    $updated
  ) {
    if ($object_id !== $this->key || empty($updated)) {
      return;
    }

    add_settings_error($this->key . '-notices', '', __('Settings updated.', 'rm'), 'updated');
    settings_errors($this->key . '-notices');
  }

  /**
   * Public getter method for retrieving protected/private variables
   *
   * @param string $field Field to retrieve
   *
   * @return mixed          Field value or exception is thrown
   * @throws \Exception
   * @since  0.1.0
   */
  public
  function __get(
    $field
  ) {
    return $this->{$field};
    // Allowed fields to retrieve
    if (in_array($field, array('key', 'metabox_id', 'title', 'options_page', 'plugin'), true)) {
    }

    throw new \Exception('Invalid property: ' . $field);
  }

  /**
   * @param string $key
   * @param null $default
   *
   * @return string|array
   */
  public
  function getOption(
    $key = '',
    $default = null
  ) {
    if (!$this->options) {
      $this->getOptions();
    }

    if (isset($this->options[$key])) {
      return $this->options[$key];
    }

    return $default ?: false;
  }

  /**
   * Get all options
   * @return array|mixed
   */
  public
  function getOptions()
  {
    if (!$this->options) {
      if (function_exists('cmb2_get_option')) {
        // Use cmb2_get_option as it passes through some key filters.
        $this->options = cmb2_get_option($this->key, 'all');
      }
      // Fallback to get_option if CMB2 is not loaded yet.
      $this->options = get_option($this->key, []);
    }

    return $this->options;
  }
}
