<?php


namespace Borgattis;


use ComposePress\Core\Abstracts\Component_0_9_0_0;

class Woocommerce extends Component_0_9_0_0 {
  public function setup() {
//    add_action( 'init', [ $this, 'remove_wc_actions' ] );
    //add_filter( 'woocommerce_checkout_fields', [ $this, 'override_checkout_fields' ], 10000 );
    //add_action( 'wp_footer', [ $this, 'format_checkout_billing_phone' ] );
    add_action( 'woocommerce_after_checkout_billing_form', [ $this, 'gift_fields' ] );
    add_action( 'woocommerce_checkout_update_order_meta', [ $this, 'save_custom_checkout_fields' ] );
    add_action( 'woocommerce_admin_order_data_after_billing_address', [ $this, 'display_admin_order_meta' ], 10, 1 );
  }

  public function remove_wc_actions() {
    remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
    remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );
  }

  public function get_tiny_cart() {
    ob_start(); ?>
    <a href="<?php echo get_permalink( $this->plugin->settings->getOption( 'wizard_page_id' ) ); ?>?show-cart=1#step3">
      <svg class="icon__cart" enable-background="new 0 0 437.812 437.812" viewBox="0 0 437.812 437.812"
           xmlns="http://www.w3.org/2000/svg">
        <circle cx="152.033" cy="390.792" r="47.02"/>
        <circle cx="350.563" cy="390.792" r="47.02"/>
        <path
          d="m114.939 82.024-16.196-49.11c-6.447-19.415-24.476-32.622-44.931-32.914h-35.004c-5.771 0-10.449 4.678-10.449 10.449s4.678 10.449 10.449 10.449h35.004c11.361.251 21.365 7.546 25.078 18.286l65.829 200.098-4.702 12.016c-5.729 14.98-4.185 31.769 4.18 45.453 8.695 13.274 23.323 21.466 39.184 21.943h203.755c5.771 0 10.449-4.678 10.449-10.449s-4.678-10.449-10.449-10.449h-203.756c-8.797-.304-16.849-5.017-21.42-12.539-4.932-7.424-5.908-16.796-2.612-25.078l6.269-15.674c.942-2.504 1.124-5.23.522-7.837l-3.135-7.837 212.637-21.943c15.482-1.393 28.327-12.554 31.869-27.69l21.943-92.473z"/>
      </svg>
      <?php echo wc_price( WC()->cart->get_subtotal() ); ?>
    </a>
    <?php
    return ob_get_clean();
  }

  public function override_checkout_fields( $fields ) {
    unset( $fields['billing']['billing_company'] );
    unset( $fields['shipping']['shipping_company'] );

    $fields['billing']['billing_first_name']['placeholder'] = 'First Name';
    $fields['billing']['billing_last_name']['placeholder']  = 'Last Name';
    $fields['billing']['billing_city']['placeholder']       = 'City';
    $fields['billing']['billing_postcode']['placeholder']   = 'Postcode';
    $fields['billing']['billing_email']['placeholder']      = $this->is_gift() ? 'Your E-mail' : 'E-mail';
    $fields['billing']['billing_phone']['placeholder']      = $this->is_gift() ? 'Your Phone' : 'Phone';
    unset( $fields['order']['order_comments'] );

    return $fields;
  }

  public function format_checkout_billing_phone() {
    if ( is_checkout() && ! is_wc_endpoint_url() ) :
      ?>
      <script type="text/javascript">
        jQuery(function ($) {
          $("#billing_phone").attr('maxlength', '12');
          $('#billing_phone').on('input focusout', function () {
            var p = $(this).val();

            p = p.replace(/[^0-9]/g, '');
            p = p.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
            $(this).val(p);
          });
        });
      </script>
    <?php
    endif;
  }

  public function gift_fields() { ?>
    <div class="mtd mtd__checkout">
      <div class="mtd__gift-header">
        <h4>Is this a gift?</h4>
      </div>
      <div class="mtd__gift-content <?php echo $this->is_gift() ? 'mtd__gift-content--active' : ''; ?>">
        <div class="row">
          <div class="wpb_column columns medium-6 small-12">
            <input type="text" name="gift_first_name" placeholder="To" value="<?php echo WC()->session->get( 'borgattis_gift_first_name' ); ?>"/>
          </div>
          <div class="wpb_column columns medium-6 small-12">
            <input type="text" name="gift_last_name" value="<?php echo WC()->session->get( 'borgattis_gift_last_name' ); ?>"/>
          </div>
          <div class="wpb_column columns medium-12">
            <textarea name="gift_message" id="" cols="30" rows="5" placeholder="Message"><?php echo WC()->session->get( 'borgattis_gift_message' ); ?></textarea>
          </div>

        </div>
      </div>


    </div>
  <?php }

  public function save_custom_checkout_fields( $order_id ) {
    if ( ! empty( $_POST['gift_first_name'] ) ) {
      update_post_meta( $order_id, '_gift_first_name', sanitize_text_field( $_POST['gift_first_name'] ) );
    }
    if ( ! empty( $_POST['gift_last_name'] ) ) {
      update_post_meta( $order_id, '_gift_last_name', sanitize_text_field( $_POST['gift_last_name'] ) );
    }
    if ( ! empty( $_POST['gift_message'] ) ) {
      update_post_meta( $order_id, '_gift_message', sanitize_text_field( $_POST['gift_message'] ) );
    }
  }

  public function display_admin_order_meta( $order ) {
    echo '<p><strong>' . __( 'Gift first name' ) . ':</strong> ' . get_post_meta( $order->get_id(), '_gift_first_name', true ) . '</p>';
    echo '<p><strong>' . __( 'Gift last name' ) . ':</strong> ' . get_post_meta( $order->get_id(), '_gift_last_name', true ) . '</p>';
    echo '<p><strong>' . __( 'Gift message' ) . ':</strong> ' . get_post_meta( $order->get_id(), '_gift_message', true ) . '</p>';
  }

  public function is_gift() {
    return WC()->session->get( 'borgattis_is_gift' );
  }
}
