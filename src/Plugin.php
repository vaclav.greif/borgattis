<?php

namespace Borgattis;

use Borgattis\Core\Plugin as PluginBase;
use Borgattis\Managers\ApiManager;
use Borgattis\Managers\RepositoriesManager;
use ComposePress\Core\Exception\ContainerInvalid;
use ComposePress\Core\Exception\ContainerNotExists;
use Exception;

/**
 * Class Plugin
 * @package Borgattis
 */
class Plugin extends PluginBase
{
  /**
   * Plugin version
   */
  const VERSION = '0.1.0';

  /**
   * Plugin slug name
   */
  const PLUGIN_SLUG = 'borgattis';

  /**
   * Plugin namespace
   */
  const PLUGIN_NAMESPACE = '\Borgattis';

  /**
   * Plugin namespace
   */
  const PLUGIN_PREFIX = '_borgattis_';

  /**
   * @var Frontend
   */
  private $frontend;

  /**
   * @var array $assets
   */
  private $assets;
  /**
   * @var RepositoriesManager
   */
  private $repositories;
  /**
   * @var RepositoriesManager
   */
  private $repositoriesManager;
  /**
   * @var ApiManager
   */
  private $apiManager;
  /**
   * @var Admin
   */
  private $admin;
  /**
   * @var Settings
   */
  private $settings;
  /**
   * @var Helpers
   */
  private $helpers;
  /**
   * @var Cart
   */
  private $cart;
  /**
   * @var WooCommerce
   */
  private $woocommerce;

  /**
   * Plugin constructor.
   *
   * @param Frontend $frontend
   * @param Admin $admin
   * @param RepositoriesManager $repositoriesManager
   * @param ApiManager $apiManager
   * @param Settings $settings
   * @param Helpers $helpers
   * @param Cart $cart
   *
   * @throws ContainerInvalid
   * @throws ContainerNotExists
   */
  public function __construct(
    Frontend $frontend,
    Admin $admin,
    RepositoriesManager $repositoriesManager,
    ApiManager $apiManager,
    Settings $settings,
    Helpers $helpers,
    Cart $cart,
    WooCommerce $woocommerce
  ) {
    $this->frontend            = $frontend;
    $this->admin               = $admin;
    $this->repositoriesManager = $repositoriesManager;
    $this->apiManager          = $apiManager;
    $this->settings            = $settings;
    $this->helpers             = $helpers;
    $this->cart                = $cart;
    $this->woocommerce         = $woocommerce;
    parent::__construct();
  }

  /**
   * @return Frontend
   */
  public function getFrontend()
  {
    return $this->frontend;
  }

  /**
   * @return RepositoriesManager
   */
  public function getRepositoriesManager(): RepositoriesManager
  {
    return $this->repositoriesManager;
  }

  /**
   * @return ApiManager
   */
  public function getApiManager(): ApiManager
  {
    return $this->apiManager;
  }

  /**
   * @return Admin
   */
  public function getAdmin(): Admin
  {
    return $this->admin;
  }

  /**
   * @return Settings
   */
  public function getSettings(): Settings
  {
    return $this->settings;
  }

  /**
   * @return Helpers
   */
  public function getHelpers(): Helpers
  {
    return $this->helpers;
  }

  /**
   * @return Cart
   */
  public function getCart(): Cart
  {
    return $this->cart;
  }

  /**
   * @return WooCommerce
   */
  public function get_woocommerce(): WooCommerce
  {
    return $this->woocommerce;
  }

  /**
   * Method to check if plugin has its dependencies. If not, it silently aborts
   * @return bool
   */
  protected function get_dependencies_exist()
  {
    return true;
  }


  /**
   * Plugin activation and upgrade
   *
   * @param $network_wide
   *
   * @return void
   */
  public function activate($network_wide)
  {
  }

  /**
   * Plugin de-activation
   *
   * @param $network_wide
   *
   * @return void
   */
  public function deactivate($network_wide)
  {
  }

  /**
   * Plugin uninstall
   * @return void
   */
  public function uninstall()
  {
  }

  /**
   * Gets asset URL from assets-manifest.json
   *
   * @param $file
   *
   * @return string
   * @throws Exception
   */
  public function asset($file): string
  {
    $manifest = $this->get_asset_path('build/assets-manifest.json');

    if (!$this->assets && file_exists($manifest)) {
      $this->assets = json_decode(file_get_contents($manifest), true);
    }

    if (isset($this->assets[$file])) {
      return $this->get_asset_url("build/{$this->assets[$file]}");
    }

    throw new Exception("Asset $file doesn't exists.");
  }

  public function getPrefix()
  {
    return $this::PLUGIN_PREFIX;
  }

}
