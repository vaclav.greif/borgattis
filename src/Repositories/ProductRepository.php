<?php

namespace Borgattis\Repositories;

use Borgattis\Core\Component;
use Borgattis\Models\Product;
use ComposePress\Core\Exception\Plugin;
use Doctrine\Common\Collections\ArrayCollection;


class ProductRepository extends Component {
  public function find( $args = [] ) {
    $defaults   = [
      'limit' => - 1,
    ];
    $args       = wp_parse_args( $args, $defaults, );
    $collection = new ArrayCollection();
    $products   = wc_get_products( $args );
    foreach ( $products as $item ) {
      $collection->add( $this->get( $item->get_id() ) );
    }

    return $collection;
  }

  /**
   * @return ArrayCollection&Product[]
   * @throws Plugin
   */
  public function all() {
    $collection = new ArrayCollection();
    $products   = wc_get_products( [ 'limit' => - 1 ] );
    foreach ( $products as $item ) {
      $collection->add( $this->get( $item->get_id() ) );
    }

    return $collection;
  }

  /**
   * @param $id
   *
   * @return Product
   * @throws Plugin
   */
  public function get( $id ) {
    $prod = $this->plugin->create_component( Product::class, $id );
    $prod->init();

    return $prod;
  }
}
