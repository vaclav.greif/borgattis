<?php

namespace Borgattis\Repositories;

use Borgattis\Core\Component;
use Borgattis\Models\CartItem;
use Doctrine\Common\Collections\ArrayCollection;


class CartItemRepository extends Component
{
  public function find($args)
  {
  }

  /**
   * @return ArrayCollection
   */
  public function all()
  {
    $collection = new ArrayCollection();

    foreach (WC()->cart->get_cart() as $key => $item) {
      $collection->add($this->get($key));
    }

    return $collection;
  }

  /**
   * @param $id
   *
   * @return CartItem
   * @throws \ComposePress\Core\Exception\Plugin
   */
  public function get($id)
  {
    $prod = $this->plugin->create_component(CartItem::class, $id);
    $prod->init();

    return $prod;
  }
}
