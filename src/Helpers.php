<?php

namespace Borgattis;


use Borgattis\Core\Component;

/**
 * @var Plugin $plugin
 */
class Helpers extends Component
{
  public function setup()
  {
  }

  public function prefixString($string)
  {
    return $this->plugin->getPrefix() . $string;
  }

  public function geocodePostcode($postcode)
  {
    $url      = add_query_arg(
      [
        'components'  => "country:US|postal_code:{$postcode}}",
        'sensor'      => 'false',
        'key'         => $this->plugin->settings->getOption('google_maps_api_key'),
        'result_type' => 'locality',
      ],
      'https://maps.google.com/maps/api/geocode/json'
    );
    $response = wp_remote_get($url);
    $result   = json_decode(wp_remote_retrieve_body($response));
    $lat      = $result->results[0]->geometry->location->lat;
    $lng      = $result->results[0]->geometry->location->lng;

    $url      = add_query_arg(
      [
        'latlng' => $lat . ',' . $lng,
        'sensor' => 'false',
        'key'    => $this->plugin->settings->getOption('google_maps_api_key'),
      ],
      'https://maps.googleapis.com/maps/api/geocode/json'
    );
    $response = wp_remote_get($url);
    $results   = json_decode(wp_remote_retrieve_body($response));

    $location = [];

    foreach ($results->results as $result) {

      foreach ($result->address_components as $component) {
        $component = (array) $component;

        switch ($component['types']) {
          case in_array('street_number', $component['types']):
            $location['street_number'] = $component['long_name'];
            break;
          case in_array('route', $component['types']):
            $location['street'] = $component['long_name'];
            break;
          case in_array('sublocality', $component['types']):
            $location['sublocality'] = $component['long_name'];
            break;
          case in_array('locality', $component['types']):
            $location['locality'] = $component['long_name'];
            break;
          case in_array('administrative_area_level_2', $component['types']):
            $location['admin_2'] = $component['long_name'];
            break;
          case in_array('administrative_area_level_1', $component['types']):
            $location['state'] = $component['short_name'];
            break;
          case in_array('postal_code', $component['types']):
            $location['postal_code'] = $component['long_name'];
            break;
          case in_array('country', $component['types']):
            $location['country'] = $component['long_name'];
            break;
        }
      }
    }

    return $location;
  }
}
