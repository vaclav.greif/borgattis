<?php

namespace Borgattis;


use Borgattis\Core\Component;

/**
 * @var Plugin $plugin
 */
class Admin extends Component
{
  public function setup()
  {
    add_action('cmb2_admin_init', [$this, 'registerMetaboxes']);
  }

  /**
   * Register Metaboxes
   */
  public function registerMetaboxes()
  {
    $prefix = $this->plugin->getPrefix();
    $cmb = new_cmb2_box(
      [
        'id'           => 'product_metabox',
        'title'        => __('Product custom details', 'borgattis'),
        'object_types' => array('product'),
        'context'      => 'normal',
        'priority'     => 'high',
        'show_names'   => true,
      ]
    );

    $cmb->add_field(
      [
        'name' => __('Popup image', 'borgattis'),
        'id'   => $prefix . 'popup_image',
        'type' => 'file',
      ]
    );

    $cmb->add_field(
      [
        'name' => __('Pairs with', 'borgattis'),
        'id'   => $prefix . 'pairing',
        'type' => 'wysiwyg',
      ]
    );
    $cmb->add_field(
      [
        'name' => __('No. of pieces per box', 'borgattis'),
        'id'   => $prefix . 'pieces_per_box_no',
        'type' => 'wysiwyg',
      ]
    );
    $cmb->add_field(
      [
        'name' => __('No. of servings', 'borgattis'),
        'id'   => $prefix . 'servings_no',
        'type' => 'text',
      ]
    );
    $cmb->add_field(
      [
        'name' => __('Ingredients', 'borgattis'),
        'id'   => $prefix . 'ingredients',
        'type' => 'wysiwyg',
      ]
    );
    $cmb->add_field(
      [
        'name' => __('How to cook', 'borgattis'),
        'id'   => $prefix . 'how_to_cook',
        'type' => 'wysiwyg',
      ]
    );
    $cmb->add_field(
      [
        'name' => __('Is frozen product?', 'borgattis'),
        'id'   => $prefix . 'is_frozen_product',
        'type' => 'checkbox',
      ]
    );
    $cmb->add_field(
      [
        'name' => __('Make their day title', 'borgattis'),
        'id'   => $prefix . 'mtd_title',
        'type' => 'text',
      ]
    );
  }
}
