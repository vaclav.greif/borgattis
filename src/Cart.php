<?php

namespace Borgattis;


use Borgattis\Core\Component;
use Borgattis\Managers\RepositoriesManager;
use Borgattis\Models\CartItem;

/**
 * @var Plugin $plugin
 */
class Cart extends Component {
  /**
   * @var RepositoriesManager
   */
  private $repositoriesManager;

  public function __construct( RepositoriesManager $repositoriesManager ) {
    $this->repositoriesManager = $repositoriesManager;
  }

  public function setup() {
    add_action( 'woocommerce_before_calculate_totals', [ $this, 'setCartItemPrices' ] );
    add_filter( 'woocommerce_get_item_data', [ $this, 'customCartItemData' ], 10, 2 );
    add_filter( 'woocommerce_cart_item_visible', [ $this, 'hideCartItems' ], 10, 3 );
    add_filter( 'woocommerce_checkout_cart_item_visible', [ $this, 'hideCartItems' ], 10, 3 );
    add_filter( 'woocommerce_before_calculate_totals', [ $this, 'changeCartItemProduct' ], 1 );
    add_action( 'template_redirect', [ $this, 'maybe_clear_cart' ] );
    add_filter( 'woocommerce_cart_contents_count', [ $this, 'alter_cart_contents_count' ], 9999, 1 );
    add_action( 'woocommerce_cart_item_removed', [ $this, 'remove_child_items' ], 9999, 1 );
    add_action( 'woocommerce_add_to_cart_redirect', [ $this, 'add_to_cart_redirect' ], 9999, 1 );
  }

  /**
   * Get Cart
   * @return \Doctrine\Common\Collections\ArrayCollection
   */
  public function getCart() {
    return $this->repositoriesManager->cartItemRepository->all();
  }


  /**
   * @param $id
   *
   * @return Models\CartItem
   * @throws \ComposePress\Core\Exception\Plugin
   */
  public function getCartItem( $id ) {
    return $this->repositoriesManager->cartItemRepository->get( $id );
  }

  /**
   * Get child items
   *
   * @param $parentId
   *
   * @return \Doctrine\Common\Collections\ArrayCollection
   */
  public function getChildItems( $parentId ) {
    return $this->repositoriesManager->cartItemRepository->all()->filter(
      function ( $entry ) use ( $parentId ) {
        return $entry->getParentId() === $parentId;
      }
    );
  }

  /**
   * Get sorted cart
   * @return array
   */
  public function getSortedCart() {
    $boxes = $this->getCart()->filter(
      function ( $item ) {
        return $item->getType() === 'box';
      }
    );

    $cart = [];

    foreach ( $boxes as $box ) {
      $cart[] = $this->getBox( $box->getId() );
    }

    return $cart;
  }

  /**
   * Add item to cart
   *
   * @param $productId
   * @param  int  $quantity
   * @param  array  $cartItemData
   * @param  int  $variationId
   * @param  array  $variation
   *
   * @return bool|string
   * @throws \Exception
   */
  public function addToCart( $productId, $quantity = 1, $cartItemData = [], $variationId = 0, $variation = [] ) {
    return WC()->cart->add_to_cart( $productId, $quantity, $variationId, $variation, $cartItemData );
  }

  /**
   * Remove item from cart
   *
   * @param $id
   *
   * @return bool
   */
  public function removeCartItem( $id ) {
    return WC()->cart->remove_cart_item( $id );
  }

  /**
   * Get a single box
   *
   * @param $id
   *
   * @return array
   */
  public function getBox( $id ) {
    return [
      'box'   => $this->repositoriesManager->cartItemRepository->get( $id ),
      'items' => $this->getChildItems( $id ),
    ];
  }

  /**
   * Set prices of box and child items
   */
  public function setCartItemPrices() {
    static $calculated;
    if ($calculated ) {
      return;
    }
    $calculated = true;
    foreach ( $this->getSortedCart() as $box ) {
      $price = 0;
      foreach ( $box['items'] as $item ) {
        $price += $item->getPrice() * $item->getQuantity();
        $item->getCartItem()['data']->set_price( 0 );
      }
      $box['box']->getCartItem()['data']->set_price( $price );
    }
  }

  /**
   * Add the custom cart item data to cart and checkout
   *
   * @param $itemData
   * @param $cartItemData
   *
   * @return array
   * @throws \ComposePress\Core\Exception\Plugin
   */
  public function customCartItemData( $itemData, $cartItemData ) {
    $item = $this->repositoriesManager->cartItemRepository->get( $cartItemData['key'] );
    if ( 'box' === $item->getType() ) {
      $description = [];
      $box         = $this->getBox( $item->getId() );
      foreach ( $box['items'] as $boxItem ) {
        $description[] = sprintf( '%sx %s', $boxItem->getQuantity(), $boxItem->getName() );
      }

      $itemData[] = [
        'key'   => 'Items',
        'value' => implode( ', ', $description ),
      ];
    }

    return $itemData;
  }

  /**
   * Hide child items from cart
   *
   * @param $visible
   * @param $cartItem
   * @param $cartItemId
   *
   * @return bool
   * @throws \ComposePress\Core\Exception\Plugin
   */
  public function hideCartItems( $visible, $cartItem, $cartItemId ) {
    $item = $this->repositoriesManager->cartItemRepository->get( $cartItemId );
    if ( $item->getParentId() ) {
      $visible = false;
    }

    return $visible;
  }

  /**
   * Set item quantity
   *
   * @param $id
   * @param $quantity
   *
   * @return bool
   */
  public function updateItemQuantity( $id, $quantity ) {
    return WC()->cart->set_quantity( $id, $quantity );
  }

  public function calculateShipping() {
    WC()->cart->calculate_shipping();
    WC()->cart->calculate_totals();

    $packages = WC()->cart->get_shipping_packages();
    $packages = WC()->shipping()->calculate_shipping( $packages );
    $rates    = [];
    foreach ( $packages as $package ) {
      foreach ( $package['rates'] as $rate ) {
        $cost = $rate->get_cost();
        foreach ( $rate->get_taxes() as $tax ) {
          $cost += $tax;
        }

        $rates[] = [
          'label' => $rate->get_label(),
          'cost'  => $cost,
        ];
      }
    }

    return $rates;
  }

  /**
   * @param $_product \WC_Product
   * @param $cartItem
   * @param $cartItemId
   */
  public function changeCartItemProduct() {
    foreach ( WC()->cart->get_cart_contents() as $item_id => $cart_item ) {
      $item = $this->repositoriesManager->cartItemRepository->get( $item_id );
      // If the item is content of the box, set weight and dimensions to 0
      if ( $item->getParentId() ) {
        $cart_item['data']->set_virtual(true);
        $cart_item['data']->set_weight( 0 );
        $cart_item['data']->set_height( 0 );
        $cart_item['data']->set_width( 0 );
        $cart_item['data']->set_length( 0 );
      } // Else this is a box item
      else {
        if (!$item->getBoxSize()) {
          continue;
        }

        $box          = $this->getBox( $item_id );
        $key          = $box['box']->getBoxType() === 'standard' ? 'box_sizes' : 'box_sizes_fettuccine';
        $extra_weight = 0;
        $frozen       = false;

        // Get the box dimensions
        foreach ( $this->plugin->getSettings()->getOption( $key ) as $boxSize ) {
          if ( $boxSize['size'] == $box['box']->getBoxSize() ) {
            $cart_item['data']->set_length( $boxSize['length'] );
            $cart_item['data']->set_height( $boxSize['height'] );
            $cart_item['data']->set_width( $boxSize['width'] );
            $extra_weight = $boxSize['extra_weight'];
          }
        }
        // Calculate the extra weight from box items
        $weight = 0;
        foreach ( $box['items'] as $item ) {
          $p      = $this->repositoriesManager->productRepository->get( $item->getProductId() );
          $weight += $p->getProduct()->get_weight( 'edit' );
          if ( $p->getDetails()['is_frozen_product'] ) {
            $frozen = true;
          }
        }

        // Add and set the extra weight
        if ( $extra_weight && $frozen ) {
          $weight += $extra_weight;
        }

        $cart_item['data']->set_weight( $weight );
      }
    }
  }

  public function maybe_clear_cart() {
    if ( ! isset( $_GET['empty-cart'] ) ) {
      return;
    }
    //WC()->cart->empty_cart();
  }

  function alter_cart_contents_count( $count ) {
    $count = 0;
    foreach ( WC()->cart->get_cart() as $key => $item ) {
      $item = $this->repositoriesManager->cartItemRepository->get( $key );
      if ( ! $item->getParentId() ) {
        $count += 1;
      }
    }

    return $count;
  }

  public function remove_child_items( $key ) {
    /** @var CartItem $item */
    foreach ( $this->getChildItems($key) as $item ) {
      $this->removeCartItem($item->getId());
    }
  }

  public function add_to_cart_redirect(  ) {
    return wc_get_cart_url();
  }
}
