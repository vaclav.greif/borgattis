<?php

namespace Borgattis\Managers;

use Borgattis\Core\Manager;
use Borgattis\Models\CartItem;
use Borgattis\Models\Product;

class ApiManager extends Manager
{

  const MODULE_NAMESPACE = '\Borgattis\Api';
  const REST_NAMESPACE = 'borgattis/v1';

  protected $modules = [
    'CartApi',
    'ProductsApi',
    'PostcodeApi',
    'BoxesApi',
    'BoxApi',
  ];

  public function getRestUrl()
  {
    return rest_url($this->getRestNamespace());
  }

  public function getRestNamespace()
  {
    return $this::REST_NAMESPACE;
  }

  public function setup()
  {
    add_action( 'init', array( $this, 'enable_wc_frontend_in_rest' ) );
  }

  /**
   * We have to tell WC that this should not be handled as a REST request.
   * Otherwise we can't use the product loop template contents properly.
   * Since WooCommerce 3.6
   *
   * @param bool $is_rest_api_request
   *
   * @return bool
   */
  public function enable_wc_frontend_in_rest($is_rest_api_request)
  {
    if ( ! WC()->is_rest_api_request() ) {
      return;
    }

    WC()->frontend_includes();

    if ( null === WC()->cart && function_exists( 'wc_load_cart' ) ) {
      wc_load_cart();
    }

    WC()->session->set_customer_session_cookie( true );
  }

  public function getApiCartItem(CartItem $cart_item)
  {
    return [
      'id'        => $cart_item->getId(),
      'quantity'  => $cart_item->getQuantity(),
      'name'      => $cart_item->getName(),
      'parentId'  => $cart_item->getParentId(),
      'productId' => $cart_item->getProductId(),
      'image'   => $cart_item->getImageUrl()
    ];
  }

  public function getApiBox(CartItem $cart_item)
  {
    return [
      'id'   => $cart_item->getId(),
      'name' => $cart_item->getName(),
      'size' => $cart_item->getBoxSize(),
      'type' => $cart_item->getBoxType(),
    ];
  }

  public function getApiProduct(Product $product)
  {
    return [
      'id'          => $product->getId(),
      'name'        => $product->getName(),
      'categories'  => $product->getCategoriesIds(),
      'details'     => $product->getDetails(),
      'imageThumb'  => $product->getImageThumb(),
      'imageLarge'  => $product->getImageLarge(),
      'popupImage'  => $product->getPopupImage(),
      'stockStatus' => $product->getStockStatus(),
    ];
  }

  public function getSortedCart()
  {
    $cart   = $this->plugin->cart->getSortedCart();
    $result = [];
    foreach ($cart as $box) {
      $items         = [];
      $totalQuantity = 0;
      foreach ($box['items'] as $item) {
        $items[]       = $this->getApiCartItem($item);
        $totalQuantity += $item->getQuantity();
      }

      $box = $this->getApiBox($box['box']);

      $result[] = [
        'box'           => $box,
        'items'         => $items,
        'itemsQuantity' => $totalQuantity,
        'isFull'        => $box['size'] == $totalQuantity,
      ];
    }

    return $result;
  }
}
