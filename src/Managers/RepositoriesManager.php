<?php

namespace Borgattis\Managers;

use Borgattis\Core\Manager;
use Borgattis\Repositories\CartItemrepository;
use Borgattis\Repositories\ProductRepository;

/**
 * Class RepositoriesManager
 * @package Borgattis\Managers
 * @property ProductRepository $productRepository
 * @property CartItemrepository $cartItemRepository
 */
class RepositoriesManager extends Manager
{

  const MODULE_NAMESPACE = '\Borgattis\Repositories';

  protected $modules = [
    'ProductRepository',
    'CartItemRepository',
  ];

}
