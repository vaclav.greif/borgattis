<?php

/* @var $container \ComposePress\Dice\Dice */

use Borgattis\Managers\RepositoriesManager;
use Borgattis\Plugin;

$container = $container->addRules(
  [
    '*'                              => [
      'shared' => true,
    ],
    \Borgattis\Models\Product::class => [
      'shared' => false,
    ],
    \Borgattis\Models\CartItem::class => [
      'shared' => false,
    ],
  ]
);
