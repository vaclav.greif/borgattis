const fs = require('fs');
const path = require('path');
const package = require('../package.json');

const folder = process.cwd().split(/[\/\\]/).pop();
const filepath = path.resolve(`./${folder}.php`);

if (fs.existsSync(filepath)) {
  const lines = fs
    .readFileSync(filepath, { encoding: 'utf8' })
    .split('\n');

  let inHeader = false;
  let isHeaderDone = false;

  const newLines = lines.map((line) => {
    if (inHeader && /\*\//.test(line)) {
      inHeader = false;
      isHeaderDone = true;
    }

    if (!inHeader && !isHeaderDone && /\/\*/.test(line)) {
      inHeader = true;
    }

    const isVersion = line.match(/\s*Version:\s*(?<version>.+)\s*/i);

    if (isVersion) {
      return line.replace(isVersion.groups.version, package.version);
    }

    return line;
  });

  fs.writeFileSync(filepath, newLines.join('\n'), { encoding: 'utf8' });
}
