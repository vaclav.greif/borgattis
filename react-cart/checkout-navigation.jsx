if (!__webpack_public_path__) {
  __webpack_public_path__ = window.borgattisCheckoutApp.publicPath;
}

import React from 'react';
import ReactDOM from 'react-dom';
import { DisconnectedNavigation } from './components/Navigation';
//import './assets/plugin.scss';

const NavigationApp = () => {
  const step = 4;

  const handleSetStep = (newStep) => {
    if (newStep < step) {
      window.location.href = window.borgattisCheckoutApp.wizard_page + '#step' + newStep;
    }
  };

  return (
    <DisconnectedNavigation step={step} setStep={handleSetStep} />
  );
};

ReactDOM.render(<>
  <NavigationApp />
  <div id="borgattis-wizard-modals"/>
</>, document.querySelector('#checkout-app'));
