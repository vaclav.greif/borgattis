if (!__webpack_public_path__) {
  __webpack_public_path__ = window.cartApp.publicPath;
}

import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import store from './store';
import App from './components/App.jsx';
//import './assets/plugin.scss';

const CartApp = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

ReactDOM.render(<CartApp />, document.querySelector('#cart-app'));
