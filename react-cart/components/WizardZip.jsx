import React, { useState } from 'react';
import PT from 'prop-types';
import { connect } from 'react-redux';
import { getZip, setZip, ZIP_INVALID, setStep } from '../store/wizard';

const WizardZip = ({ zip, setZip, setStep }) => {
  const [currentZip, setCurrentZip] = useState(zip.value);
  const handleCurrentZip = event => setCurrentZip(event.target.value);

  const handleSubmit = (event) => {
    event.preventDefault();
    setZip(currentZip).then(data => data.payload.valid && setStep(2));
  };

  return (
    <div className="content-wrap">
      <h1>Welcome to the new Borgatti&apos;s</h1>

      <div className="intro">
        <div className="intro__zip">
          <p className="intro__zip-text intro__zip-text--uppercase">How to place an order</p>
          <p className="intro__zip-text">
            First, select your box size<br />
            Next, build your box<br />
            Then, add your box to cart
          </p>
          <p className="intro__zip-text">Build as many boxes as you like</p>

          <form className="intro__zip-form" onSubmit={handleSubmit}>
            <label htmlFor="input-zip-code" className="intro__zip-form__label">ZIP code</label>
            <input
              type="text"
              id="input-zip-code"
              className="intro__zip-form__input"
              placeholder="Enter Zip Code"
              value={currentZip}
              onChange={handleCurrentZip}
            />
            {zip.state === ZIP_INVALID && (
              <label htmlFor="input-zip-code" className="intro__zip-form__label">ZIP is not valid, please check the input.</label>
            )}
            <input type="submit" value="Get started" className="intro__zip-form__submit" />
          </form>
        </div>
        <div className="intro__image">
          <img src={window.cartApp.introImage} alt=""/>
        </div>
      </div>
    </div>
  );
};

WizardZip.propTypes = {
  zip: PT.object,
  setZip: PT.func,
  setStep: PT.func,
};

const mapStateToProps = (state) => ({
  zip: getZip(state),
});

const mapDispatchToProps = {
  setZip,
  setStep,
};

export default connect(mapStateToProps, mapDispatchToProps)(WizardZip);
