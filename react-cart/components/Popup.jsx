import React, {useRef, useLayoutEffect} from 'react';
import { createPortal } from 'react-dom';
import PT from 'prop-types';
import dialogPolyfill from 'dialog-polyfill';
import classnames from 'classnames';

const Popup = ({shown, onHide, children, extraClass,setIsPopupShown}) => {
  const dialog = useRef();

  useLayoutEffect(() => {
    if (dialog.current) {
      dialogPolyfill.registerDialog(dialog.current);
    }
  });

  useLayoutEffect(() => {
    const element = dialog.current;

    if (shown && element) {
      document.body.style.overflow = 'hidden';
    } else if (element) {
      document.body.style.overflow = 'auto';
    }
  }, [shown, dialog]);

  const hidePopup = () => {
    setIsPopupShown(false);
  };

  useLayoutEffect(() => {
    const element = dialog.current;

    if (element) {
      element.addEventListener('close', onHide);
    }

    return () => {
      if (element) {
        element.removeEventListener('close', onHide);
      }
    }


  });

  const container = document.getElementById('borgattis-wizard-modals');

  if (!container) {
    return null;
  }

  return createPortal((
    <>
      {shown && (
        <div className="modal-popup-backdrop" onClick={hidePopup}/>
      )}
      <dialog
        className={classnames('modal-popup', {
          [extraClass]: `modal-popup--${extraClass}`,
        })}
        ref={dialog}
        open={shown}
      >
        <div className="modal-popup__close-wrap">
          <button className="modal-popup__close" id="product_detail_popup_close" onClick={onHide}>
            <svg viewBox="0 0 365.7 365.7" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M243 183L356 70c13-13 13-33 0-46L341 9a32 32 0 00-45 0L183 123 70 9a32 32 0 00-46 0L9 24a32 32 0 000 46l114 113L9 296a32 32 0 000 45l15 15c13 13 33 13 46 0l113-113 113 113c12 13 33 13 45 0l15-15c13-12 13-33 0-45zm0 0"/>
            </svg>
          </button>
        </div>

        <div className="modal-popup__inner">
          {children}
        </div>
      </dialog>
    </>
  ), container);
};

Popup.propTypes = {
  shown: PT.bool,
  onHide: PT.func,
  children: PT.element,
  extraClass: PT.string,
  setIsPopupShown: PT.func,
};

export default Popup;
