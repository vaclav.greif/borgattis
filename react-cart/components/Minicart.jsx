import React from 'react';
import PT from "prop-types";
import {getCartIsShown, toggleCart} from "../store/cart";
import {connect} from "react-redux";
import {getCurrentBox, getBoxes, getIsCurrentBoxFull} from "../store/cart";
import {getStep} from "../store/wizard";

const Minicart = ({toggleCart, isShown, currentBox, boxes, isCurrentBoxFull, currentStep}) => {
  if (isShown || !currentBox || (currentStep == 1 && !isCurrentBoxFull)) {
    return null;
  }

  return (
    <div className="nav-cart" onClick={() => toggleCart(true)}>
      {currentBox !== null && currentBox.items.length &&
      <div className="nav-cart__image">
        <img src={currentBox.items.slice(-1)[0].image}/>
      </div>
      }
      <div className="nav-cart__short-text">{currentBox?.itemsQuantity}</div>
      <div className="nav-cart__long-text">{
        boxes.length > 1 ?
          <div>Your boxes</div> :
          <div>Your box {currentBox?.itemsQuantity}/{currentBox?.box?.size}</div>
      }</div>
    </div>
  );
};

Minicart.propTypes = {
  toggleCart: PT.func,
  isShown: PT.bool,
  getCurrentBox: PT.func,
  currentBox: PT.object,
  boxes: PT.array,
  getStep: PT.func,
  getIsCurrentBoxFull: PT.func,
  currentStep: PT.number,
  isCurrentBoxFull: PT.bool,
};

const mapStateToProps = (state) => ({
  isShown: getCartIsShown(state),
  currentBox: getCurrentBox(state),
  boxes: getBoxes(state),
  isCurrentBoxFull: getIsCurrentBoxFull(state),
  currentStep: getStep(state),
});

const mapDispatchToProps = {
  toggleCart,
};

export default connect(mapStateToProps, mapDispatchToProps)(Minicart);

