import React from 'react';
import PT from 'prop-types';
import classnames from 'classnames';

const NavigationStep = ({ step, title, currentStep, isLast = false, onClick }) => {
  const handleClick = () => onClick && onClick(step);

  return (
    <button
      className={classnames('nav-steps__item', {
        'nav-steps__item--is-active': currentStep === step,
        'nav-steps__item--last': isLast,
      })}
      onClick={handleClick}
      disabled={
        (currentStep === 3 && (step === 4))
        || (currentStep === 1 && (step === 2 || step === 3 || step === 4))
        || (currentStep === 2 && step === 3 || step === 4 )
      }
    >
      <span className="nav-steps__num-step">{step}</span>
      <span className="nav-steps__heading">{title}</span>
    </button>
  )
};

NavigationStep.propTypes = {
  step: PT.number,
  title: PT.string,
  currentStep: PT.number,
  isLast: PT.bool,
  onClick: PT.func,
};

export default NavigationStep;
