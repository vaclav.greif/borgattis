import React, {Fragment, useRef, useEffect} from 'react';
import PT from 'prop-types';
import {connect} from 'react-redux';
import {getAvailableCategories} from '../store/categories';
import {getAvailableproducts, getBestsellers} from '../store/products';
import Product from "./Product";
import Glider from "glider-js";
import {getCurrentBoxType} from "../store/cart";

const WizardProduct = ({categories, products, bestSellers, currentBoxType}) => {
  const handleCategoryClick = (termId) => (event) => {
    event.preventDefault();
    const category = document.getElementById(`category-${termId}`);
    if (category) {
      category.scrollIntoView({
        behavior: 'smooth'
      });
    }
  };

  const glider = useRef();

  const availableCategories = categories.map(c => c.slug).includes(currentBoxType)
    ? categories.filter(c => c.slug === currentBoxType)
    : categories;

  const availableCategoriesIds = availableCategories.map(c => c.term_id);

  useEffect(() => {
    let gliderJs;
    if (glider.current) {
      gliderJs = new Glider(glider.current, {
        slidesToShow: 'auto',
        exactWidth: true,
        itemWidth: 250, // Needed because of slidesToShow is 'auto'
        draggable: true,
        arrows: {
          prev: '.glider-prev',
          next: '.glider-next'
        },
        responsive: [
          {
            breakpoint: 920,
            settings: {
              itemWidth: 300,
            }
          }, {
            breakpoint: 1100,
            settings: {
              itemWidth: 350,
            }
          }
        ]
      });
    }

    return () => {
      if (gliderJs) {
        gliderJs.destroy();
      }
    };
  }, []);

  return (
    <div>
      <div className="content-wrap">
        <h1>Start building your box</h1>
      </div>
      <div className="type-filter">
        <div className="type-filter__wrap content-wrap">
          {availableCategories.map((category) => (
            <button className="type-filter__item" key={category.term_id}
                    onClick={handleCategoryClick(category.term_id)}>{category.name}</button>
          ))}
        </div>
      </div>
      <div className="pasta-slider content-wrap glider-contain">
        <div className="pasta-slider__heading">Best Sellers</div>

        <div className="pasta-slider__content glider" ref={glider}>
          {bestSellers.map((product) => {
            return product.categories.some(r=> availableCategoriesIds.includes(r)) && (
              <Product
                key={product.id}
                product={product}
              />
            )
          })}
        </div>

        <button role="button" aria-label="Previous" className="pasta-slider__prev glider-prev">
          <svg xmlns="http://www.w3.org/2000/svg" width="19" height="32">
            <path fill="#888888"
                  d="M.3 16.4l15.2 14.7c.3.2.5.3.8.3.3 0 .5 0 .7-.3l1.7-1.5c.2-.3.3-.5.3-.8a1 1 0 00-.3-.7L5.8 15.7 18.7 3.3a1 1 0 000-1.4L17 .3a1 1 0 00-.7-.3 1 1 0 00-.8.3L.3 15a1 1 0 00-.3.7c0 .3.1.5.3.8z"/>
          </svg>
        </button>
        <button role="button" aria-label="Next" className="pasta-slider__next glider-next">
          <svg xmlns="http://www.w3.org/2000/svg" width="19" height="32">
            <path fill="#888888"
                  d="M.3 16.4l15.2 14.7c.3.2.5.3.8.3.3 0 .5 0 .7-.3l1.7-1.5c.2-.3.3-.5.3-.8a1 1 0 00-.3-.7L5.8 15.7 18.7 3.3a1 1 0 000-1.4L17 .3a1 1 0 00-.7-.3 1 1 0 00-.8.3L.3 15a1 1 0 00-.3.7c0 .3.1.5.3.8z"/>
          </svg>
        </button>
      </div>

      {availableCategories.map((category) => (
        <Fragment key={category.term_id}>
          <h2 id={`category-${category.term_id}`} className="pasta-type-list__heading">{category.name}</h2>
          <div className="content-wrap">
            <div className="pasta-type-list">
              {products
                .filter(product => product.categories.includes(category.term_id))
                .map((product) => (
                  <Product
                    key={product.id}
                    product={product}
                  />
                ))
              }
            </div>
          </div>
        </Fragment>
      ))}
    </div>
  );
};

WizardProduct.propTypes = {
  categories: PT.array,
  products: PT.array,
  bestSellers: PT.array,
  currentBoxType: PT.string,
};

const mapStateToProps = (state) => ({
  categories: getAvailableCategories(state),
  products: getAvailableproducts(state),
  bestSellers: getBestsellers(state),
  currentBoxType: getCurrentBoxType(state)
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(WizardProduct);

