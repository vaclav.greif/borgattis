import React from 'react';
import PT from 'prop-types';
import ButtonMinus from "./ButtonMinus";
import ButtonPlus from "./ButtonPlus";

const AddToCart = ({quantity, onAddToCart, onQuantityPlus, onQuantityMinus, buttonType}) => {
  return quantity > 0 ? (
    <div className="pasta-type-list__buttons">
      <ButtonMinus
        className="pasta-type-list__button"
        onClick={onQuantityMinus}
        buttonType={buttonType}
      />
      <span className="pasta-type-list__counter">
          {quantity}
        </span>
      <ButtonPlus
        className="pasta-type-list__button"
        onClick={onQuantityPlus}
        buttonType='plus'
      />
    </div>
  ) : (
    <ButtonPlus
      className={buttonType === 'button' ? 'popup-product__button' : 'pasta-type-list__button'}
      onClick={onAddToCart}
      buttonType={buttonType}
    />
  );
};

AddToCart.propTypes = {
  quantity: PT.number,
  onAddToCart: PT.func,
  onQuantityPlus: PT.func,
  onQuantityMinus: PT.func,
  buttonType: PT.string
};

export default AddToCart;
