import React from 'react';

const ButtonMinus = (props) => {
  return (
    <button {...props}>
      <svg viewBox="0 0 551.1 551.1" xmlns="http://www.w3.org/2000/svg"><path d="M276 0a276 276 0 100 552 276 276 0 000-552zm0 517a241 241 0 110-483 241 241 0 010 483z"></path><path d="M138 258h275v35H138z"></path></svg>
    </button>
  );
};

export default ButtonMinus;
