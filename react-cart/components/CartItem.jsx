import React from 'react';
import PropTypes from 'prop-types';
import AddToCart from "./AddToCart";
import {fetchUpdateQuantity} from "../store/cart";
import {getCurrentBoxId} from "../store/cart";
import {connect} from "react-redux";

const CartItem = ({item, fetchUpdateQuantity, currentBox}) => {
  const handleChangeQuantity = (id, quantity) => () => {
    fetchUpdateQuantity({id, quantity});
  };

  return (
    <div>
      <div className="checkout-panel__box-item">
        <div className="checkout-panel__box-item__image"><img
          src={item.image}/></div>
        <div className="checkout-panel__box-item__name">{item.name}</div>
        <AddToCart
          onQuantityPlus={handleChangeQuantity(item.id, item.quantity + 1, currentBox)}
          onQuantityMinus={handleChangeQuantity(item.id, item.quantity - 1, currentBox)}
          quantity={item.quantity}
        />
      </div>
    </div>
  );
};

CartItem.propTypes = {
  item: PropTypes.object,
  onQuantityPlus: PropTypes.func,
  onQuantityMinus: PropTypes.func,
  fetchUpdateQuantity: PropTypes.func,
  currentBox: PropTypes.string
};

const mapStateToProps = (state) => ({
  currentBox: getCurrentBoxId(state)
});

const mapDispatchToProps = {
  fetchUpdateQuantity,
};

export default connect(mapStateToProps, mapDispatchToProps)(CartItem);
