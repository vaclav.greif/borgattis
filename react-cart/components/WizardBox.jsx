import React from 'react';
import PT from 'prop-types';
import {connect} from 'react-redux';
import {_n, sprintf} from '@wordpress/i18n';
import {getAvailableBoxes} from '../store/boxes';
import {fetchAddBox, getIsCurrentBoxFull, fetchChangeBoxSize, getCurrentBoxId} from '../store/cart';
import {setStep} from '../store/wizard';

const WizardBox = ({boxes, fetchAddBox, setStep, isCurrentBoxFull, fetchChangeBoxSize, currentBoxId}) => {
  const sizes = boxes.standard.map(box => box.size);
  const minSize = Math.min(...sizes);
  const maxSize = Math.max(...sizes);
  const stepSize = 45 / (maxSize - minSize + 1);

  const getWidth = (size) => 55 + stepSize * size;
  const handleOnClick = (size, boxType) => () => {
    if (isCurrentBoxFull || !currentBoxId) {
      fetchAddBox({size, boxType})
        .then(() => setStep(3));
    } else {
      fetchChangeBoxSize({id: currentBoxId, size})
        .then(() => setStep(3));
    }
  };

  return (
    <div className="content-wrap">
      <h1>Pick your box</h1>

      <h2 dangerouslySetInnerHTML={{__html: "SHOP RAVIOLI AND ALL OTHER BORGATTI'S PRODUCTS"}} />

      <div className="pick-box">
        {boxes.standard.map((box) => (
          <button key={box.size} className="pick-box__box-item" onClick={handleOnClick(box.size, 'standard')}>
            <div className="pick-box__image">
              <svg data-name="Layer 3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128"
                   style={{width: `${getWidth(box.size)}%`}}>
                <path
                  d="M118.823 51.763l-14.333-6.238 12.129-5.1a1.631 1.631 0 000-3.006L82.373 22.954a1.63 1.63 0 00-1.248-.008L63.98 29.92l-17.142-6.474a1.631 1.631 0 00-1.21.023L11.379 37.936a1.631 1.631 0 000 3.006l12.129 5.1-14.331 6.237a1.631 1.631 0 00-.011 2.985l16.874 7.512c0 .025-.014.045-.014.07v26.36a1.631 1.631 0 001.032 1.517l36.309 14.324.036.014a1.63 1.63 0 001.2 0l.036-.014 36.309-14.324a1.631 1.631 0 001.032-1.517V62.252l16.857-7.5a1.631 1.631 0 00-.011-2.985zm-37.1-25.541l30.06 12.7L99.712 44H99.7L68.386 31.648zM64 33.424l32.052 12.648-32.06 13.672L31.6 46.209zm34.716 15.058v11.652l-17.864 7.954-12.966-6.459zm-82.5-9.047l30.079-12.708L59.474 31.7l-31.8 12.548zM27.3 47.947l32.883 13.741-13.059 6.906-33.25-14.8zm1.986 16.275L46.524 71.9a1.63 1.63 0 001.425-.049l14.422-7.631v36.928L29.287 88.1zM98.716 88.1l-33.084 13.047v-37l14.454 7.2a1.63 1.63 0 001.39.03l17.24-7.677zm3.261-29.413V47.988l12.146 5.287z"/>
              </svg>
            </div>
            <div className="pick-box__title">
              {sprintf(
                _n('choose %s item', 'choose %s items', box.size, 'borgattis'),
                box.size
              )}
            </div>
          </button>
        ))}
      </div>

      <div className="pick-box-wrap">
        <h2>Are you interested in just our Fettuccine?</h2>
        <h3>Shop our dried Fettuccine boxes</h3>

        <div className="pick-box">

          {boxes.fettuccine.map((box) => (
            <a key={box.size} href="#" className="pick-box__dry-item"
               onClick={handleOnClick(box.size, 'fettuccine')}>{box.name}</a>
          ))}
        </div>
      </div>
    </div>
  );
};

WizardBox.propTypes = {
  boxes: PT.array,
  fetchAddBox: PT.func,
  fetchChangeBoxSize: PT.func,
  setStep: PT.func,
  getIsCurrentBoxFull: PT.func,
  isCurrentBoxFull: PT.bool,
  getCurrentBoxId: PT.func,
  currentBoxId: PT.string,

};

const mapStateToProps = (state) => ({
  boxes: getAvailableBoxes(state),
  isCurrentBoxFull: getIsCurrentBoxFull(state),
  currentBoxId: getCurrentBoxId(state),
});

const mapDispatchToProps = {
  fetchAddBox,
  setStep,
  fetchChangeBoxSize
};

export default connect(mapStateToProps, mapDispatchToProps)(WizardBox);
