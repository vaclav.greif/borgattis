import React, {useEffect} from 'react';
import PT from 'prop-types';
import {connect} from 'react-redux';
import Navigation from './Navigation';
import Minicart from './Minicart';
import WizardZip from './WizardZip';
import WizardBox from './WizardBox';
import {getStep} from '../store/wizard';
import WizardProduct from './WizardProduct';
import Cart from "./Cart";

const App = ({step}) => {
  let Screen = () => <div/>;

  useEffect(() => {
    document.querySelectorAll('.header').forEach((el) => {
      el.classList.add('wizzard-display-none');
    });

    return () => {
      document.querySelectorAll('.header').forEach((el) => {
        el.classList.remove('wizzard-display-none');
      });
    };
  }, []);

  useEffect(() => {
    if (step == 2) {
      window.scrollTo({top: 0, behavior: 'smooth'});
    }
  }, [step]);

  if (step === 1) {
    Screen = WizardZip;
  } else if (step === 2) {
    Screen = WizardBox;
  } else if (step === 3) {
    Screen = WizardProduct;
  }

  return (
    <div className="borgattis-wizard">
      <Navigation/>
      <Minicart/>
      <Screen/>
      <Cart/>
      <div id="borgattis-wizard-modals"/>
    </div>
  );
};

App.propTypes = {
  step: PT.number,
};

const mapStateToProps = (state) => ({
  step: getStep(state),
});

export default connect(mapStateToProps)(App);
