import React, {useState} from 'react';
import PT from 'prop-types';
import {fetchAddToCart, fetchUpdateQuantity, getCartQuantities} from "../store/cart";
import {connect} from "react-redux";
import Popup from "./Popup";
import {getCurrentBoxId} from "../store/cart";
import AddToCart from "./AddToCart";


const Product = ({product, fetchAddToCart, fetchUpdateQuantity, currentBox, quantities}) => {
  const [isPopupShown, setIsPopupShown] = useState(false);

  const handleAddToCart = (productId, quantity, parentId) => () => {
    fetchAddToCart({productId, quantity, parentId});
  };

  const handleChangeQuantity = (id, quantity) => () => {
    fetchUpdateQuantity({id, quantity});
  };

  const handleShowProductDetail = (event) => {
    event.preventDefault();
    setIsPopupShown(true);
  };

  const handleHideProductDetail = (event) => {
    event.preventDefault();
    setIsPopupShown(false);
  };

  const quantity = ((quantities[currentBox] || {})[product.id] || {}).quantity;
  const cartItemId = ((quantities[currentBox] || {})[product.id] || {}).id;

  return (
    <div key={product.id} className="pasta-type-list__item pasta-type-list__item--in-slider">
      <div className="pasta-type-list__image" onClick={handleShowProductDetail}>
        <img src={product.imageThumb.url} width={product.imageThumb.width} height={product.imageThumb.height}/>
      </div>
      <span className="pasta-type-list__name">{product.name}</span>
      {
        product.stockStatus === 'instock' ?
          <AddToCart
            onAddToCart={handleAddToCart(product.id, 1, currentBox)}
            onQuantityPlus={handleChangeQuantity(cartItemId, quantity + 1, currentBox)}
            onQuantityMinus={handleChangeQuantity(cartItemId, quantity - 1, currentBox)}
            quantity={quantity}
            buttonType='plus'
          /> : <div>Out of stock</div>
      }

      <Popup
        shown={isPopupShown}
        onHide={handleHideProductDetail}
        setIsPopupShown={setIsPopupShown}
      >
        <div className="popup-product">
          <div className="popup-product__image">
            <img className="popup-product__image--main" src={product.imageLarge.url} width={product.imageLarge.width} height={product.imageLarge.height}/>
            {product.popupImage !== 'undefined' &&
            <img className="popup-product__image--secondary" src={product.popupImage.url} width={product.popupImage.width} height={product.popupImage.height}/>
            }
          </div>
          <span className="popup-product__heading">{product.name}</span>
          <span className="popup-product__price">{product.price}</span>
          <span className="popup-product__text"
                dangerouslySetInnerHTML={{__html: product.details.pieces_per_box_no}}></span>
          <span className="popup-product__servings">
          <span className="popup-product__servings__amount">{product.details.servings_no}</span> servings</span>
          {
            product.stockStatus === 'instock' ?
              <AddToCart
                onAddToCart={handleAddToCart(product.id, 1, currentBox)}
                onQuantityPlus={handleChangeQuantity(cartItemId, quantity + 1, currentBox)}
                onQuantityMinus={handleChangeQuantity(cartItemId, quantity - 1, currentBox)}
                quantity={quantity}
                buttonType='button'
              /> : <div>Out of stock</div>
          }

          <div className="popup-product__pairing">
            <p dangerouslySetInnerHTML={{__html: product.details.pairing}}></p>

            {product.details.is_frozen_product &&
            <p className="popup-product__frozen">
              <svg className="popup-product__snowflake" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                <path
                  d="M502 246h-42l10-10a10 10 0 00-14-15l-25 25h-44l31-30a10 10 0 00-15-15l-44 45h-79l56-56h63a10 10 0 000-20h-43l31-31h63a10 10 0 000-20h-43l30-30a10 10 0 00-14-14l-30 30V62a10 10 0 00-20 0v63l-31 31v-43a10 10 0 00-20 0v63l-56 56v-79l45-44a10 10 0 00-15-15l-30 31V81l45-45a10 10 0 00-15-14l-30 30V10a10 10 0 00-20 0v42l-30-30a10 10 0 00-15 14l45 45v44l-30-31a10 10 0 00-15 15l45 44v79l-56-56v-63a10 10 0 00-20 0v43l-31-31V62a10 10 0 00-20 0v43L89 75a10 10 0 00-14 14l30 30H62a10 10 0 000 20h63l31 31h-43a10 10 0 000 20h63l56 56h-79l-44-45a10 10 0 00-15 15l31 30H81l-45-45a10 10 0 00-14 15l30 30H10a10 10 0 000 20h42l-30 30a10 10 0 0014 15l45-45h44l-31 30a10 10 0 0015 15l44-45h79l-56 56h-63a10 10 0 000 20h43l-31 31H62a10 10 0 000 20h43l-30 30a10 10 0 0014 14l30-30v43a10 10 0 0020 0v-63l31-31v43a10 10 0 0020 0v-63l56-56v79l-45 44a10 10 0 0015 15l30-31v44l-45 45a10 10 0 0015 14l30-30v42a10 10 0 0020 0v-42l30 30a10 10 0 0015 0c3-4 3-10 0-14l-45-45v-44l30 31a10 10 0 0015 0c3-4 3-11 0-15l-45-44v-79l56 56v63a10 10 0 0020 0v-43l31 31v63a10 10 0 0020 0v-43l30 30a10 10 0 0014 0c4-4 4-10 0-14l-30-30h43a10 10 0 000-20h-63l-31-31h43a10 10 0 000-20h-63l-56-56h79l44 45a10 10 0 0015 0c3-4 3-11 0-15l-31-30h44l45 45c2 2 4 2 7 2s5 0 7-2c4-4 4-11 0-15l-30-30h42a10 10 0 000-20z"/>
                <path d="M483 246c-13 0-13 20 0 20s13-20 0-20z"/>
              </svg>
              This is a frozen product
            </p>
            }

          </div>
          <details className="popup-product__details">
            <summary>Ingredients</summary>
            <div dangerouslySetInnerHTML={{__html: product.details.ingredients}}></div>
          </details>

          <details className="popup-product__details">
            <summary>How to cook</summary>
            <div dangerouslySetInnerHTML={{__html: product.details.how_to_cook}}></div>
          </details>
        </div>
      </Popup>
    </div>);
};

Product.propTypes = {
  product: PT.obj,
  fetchAddToCart: PT.func,
  fetchUpdateQuantity: PT.func,
  currentBox: PT.string,
  quantities: PT.object
};

const mapStateToProps = (state) => ({
  currentBox: getCurrentBoxId(state),
  quantities: getCartQuantities(state),
});

const mapDispatchToProps = {
  fetchAddToCart,
  fetchUpdateQuantity,
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
