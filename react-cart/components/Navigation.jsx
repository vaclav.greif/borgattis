import React from 'react';
import PT from 'prop-types';
import {connect} from 'react-redux';
import {__} from '@wordpress/i18n';
import {getStep, setStep} from '../store/wizard';
import NavigationStep from './NavigationStep';
import classnames from 'classnames';
import {getAreBoxesFull, toggleCart} from '../store/cart';

const donothing = () => null;

export const DisconnectedNavigation = ({step, setStep = donothing, areBoxesFull, toggleCart}) => {
  console.log(areBoxesFull);
  const handleBack = () => {
    if (step === 1) {
      window.location.href = '/';
    }
    setStep(Math.max(step - 1, 1))
  };

  const handleStepChange = (step) => {
    if ((step === 2 || step === 3) && !areBoxesFull) {
      toggleCart(true);
    }
    setStep(step);
  }

  const steps = [
    __('Welcome', 'borgattis'),
    __('Select box', 'borgattis'),
    __('Build your box', 'borgattis'),
    __('Checkout', 'borgattis'),
  ];

  return (
    <div className="nav content-wrap">
      <div className="nav__home">
        <a href="/?empty-cart">HOME</a>
      </div>
      <div className="nav__wrap">
        <div
          className={classnames('nav-back')}>
          <button onClick={handleBack}>
            <svg xmlns="http://www.w3.org/2000/svg" width="19" height="32">
              <path
                d="M.3 16.4l15.2 14.7c.3.2.5.3.8.3.3 0 .5 0 .7-.3l1.7-1.5c.2-.3.3-.5.3-.8a1 1 0 00-.3-.7L5.8 15.7 18.7 3.3a1 1 0 000-1.4L17 .3a1 1 0 00-.7-.3 1 1 0 00-.8.3L.3 15a1 1 0 00-.3.7c0 .3.1.5.3.8z"/>
            </svg>
          </button>
        </div>
        <div className="nav-steps">
          {steps.map((title, index, steps) => (
            <NavigationStep
              key={title}
              step={index + 1}
              title={title}
              currentStep={step}
              onClick={handleStepChange}
              isLast={steps.length === (index + 1)}
            />
          ))}
        </div>
      </div>
    </div>
  )
};

DisconnectedNavigation.propTypes = {
  step: PT.number,
  setStep: PT.func,
  areBoxesFull: PT.bool,
  toggleCart: PT.func,
};

const mapStateToProps = (state) => ({
  step: getStep(state),
  areBoxesFull: getAreBoxesFull(state),
});

const mapDispatchToProps = {
  setStep,
  toggleCart,
};

export default connect(mapStateToProps, mapDispatchToProps)(DisconnectedNavigation);
