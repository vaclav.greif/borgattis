import React, {useState} from 'react';
import PT from 'prop-types';
import CartItem from "./CartItem";
import {connect} from "react-redux";
import {fetchEmptyBox, setCurrentBox, getCurrentBoxId, fetchChangeBoxSize, fetchRemoveBox} from "../store/cart";
import {getAvailableBoxes} from '../store/boxes';
import classnames from 'classnames';
import Popup from "./Popup";

const CartBox = ({box, fetchEmptyBox, setCurrentBox, currentBoxId, boxes, fetchChangeBoxSize, fetchRemoveBox}) => {
  const boxType = box.box.type;

  const [isPopupShown, setIsPopupShown] = useState(false);
  const handleEmptyBox = () => {
    fetchEmptyBox({id: box.box.id});
  };

  const handleRemoveBox = () => {
    fetchRemoveBox({id: box.box.id});
  };

  const handleShowBoxSizeChange = (event) => {
    event.preventDefault();
    setIsPopupShown(true);
  };

  const handleHideBoxSizeChange = (event) => {
    event.preventDefault();
    setIsPopupShown(false);
  };

  const handleChangeBoxSize = (size) => () => {
    fetchChangeBoxSize({id: currentBoxId, size})
      .then(function () {
        setIsPopupShown(false);
      });

  };

  return (
    <div className={classnames(
      "checkout-panel__box",
      {"checkout-panel__box--active": box.box.id === currentBoxId}
    )} onClick={() => setCurrentBox(box.box.id)}>
      <div className="checkout-panel__box-header">
        <div className="checkout-panel__box-status">Build a box {box.itemsQuantity} / {box.box.size}</div>
        <div className="checkout-panel__box-actions">
          <button onClick={handleShowBoxSizeChange}>Change box size</button>
          <br/>
          <button onClick={handleEmptyBox}>Empty box</button>
          <br/>
          <button onClick={handleRemoveBox}>Remove box</button>
        </div>
      </div>
      <Popup
        shown={isPopupShown}
        onHide={handleHideBoxSizeChange}
        extraClass={classnames("change-box-size", `change-box-size--${boxes[boxType].length}`)}
        setIsPopupShown={setIsPopupShown}
      >
        {boxes[boxType].map((item) => (
          <button
            className='checkout-panel__change-size--item checkout-panel__box-status checkout-panel__box-change-size'
            key={item.size}
            onClick={handleChangeBoxSize(item.size)}>{item.name}</button>
        ))}
      </Popup>
      {box.items.map((item) => (
        <CartItem
          key={item.id}
          item={item}
        />
      ))}

    </div>
  );
};

CartBox.propTypes = {
  box: PT.obj,
  fetchEmptyBox: PT.func,
  setCurrentBox: PT.func,
  currentBoxId: PT.string,
  boxes: PT.object,
  fetchChangeBoxSize: PT.func,
  fetchRemoveBox: PT.func,
};

const mapStateToProps = (state) => ({
  currentBoxId: getCurrentBoxId(state),
  boxes: getAvailableBoxes(state)
});

const mapDispatchToProps = {
  fetchEmptyBox,
  setCurrentBox,
  fetchChangeBoxSize,
  fetchRemoveBox
};

export default connect(mapStateToProps, mapDispatchToProps)(CartBox);

