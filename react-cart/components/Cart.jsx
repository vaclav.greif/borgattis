import React, {useEffect} from 'react';
import PT from 'prop-types';
import {connect} from "react-redux";
import {toggleCart, getCartIsShown, getBoxes} from "../store/cart";
import CartBox from "./CartBox";
import {getAreBoxesFull} from "../store/cart";
import {setStep, getStep} from "../store/wizard";

const Cart = ({toggleCart, isShown, boxes, setStep, areBoxesFull, currentStep}) => {
  const handleAddAnotherBox = () => {
    setStep(2);
    toggleCart(false);
  };

  const handleFillYourBox = () => {
    setStep(3);
    toggleCart(false);
  };


  const handleClose = () => {
    toggleCart(false);
  };

  useEffect(() => {
    const handleKeyPress = (event) => {
      if (event.code === 'Escape') {
        toggleCart(false);
      }
    };

    window.addEventListener('keyup', handleKeyPress);

    return () => {
      document.removeEventListener('keyup', handleKeyPress);
    };
  }, [toggleCart]);

  useEffect(() => {
    if (!boxes.length && currentStep === 3) {
      toggleCart(false);
      setStep(2);
    }
  }, [boxes, setStep, toggleCart, currentStep]);


  if (!isShown) {
    return null;
  }


  return (
    <div>
      <div className="checkout-panel__backdrop" onClick={handleClose}></div>
      <div className="checkout-panel">
        <button className="checkout-panel__close" id="product_detail_popup_close" onClick={handleClose}>
          <svg viewBox="0 0 365.7 365.7" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M243 183L356 70c13-13 13-33 0-46L341 9a32 32 0 00-45 0L183 123 70 9a32 32 0 00-46 0L9 24a32 32 0 000 46l114 113L9 296a32 32 0 000 45l15 15c13 13 33 13 46 0l113-113 113 113c12 13 33 13 45 0l15-15c13-12 13-33 0-45zm0 0"/>
          </svg>
        </button>

        <div className="checkout-panel__cart">
          <div className="checkout-panel__cart-icon">
            <svg viewBox="0 -31 512 512" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M165 300h272c7 0 13-4 14-11l60-210a15 15 0 00-14-19H130l-10-48c-2-7-8-12-15-12H15a15 15 0 100 30h78l54 244a45 45 0 0018 86h272a15 15 0 100-30H165a15 15 0 010-30zM477 90l-51 180H177L137 90zm0 0"/>
              <path
                d="M150 405a45 45 0 1090 0 45 45 0 00-90 0zm45-15a15 15 0 110 30 15 15 0 010-30zm0 0M362 405a45 45 0 1090 0 45 45 0 00-90 0zm45-15a15 15 0 110 30 15 15 0 010-30zm0 0"/>
            </svg>
          </div>
        </div>
        <div className="checkout-panel__items">
          {boxes.map((box) => (
            <CartBox
              key={box.box.id}
              box={box}
            />
          ))}

        </div>
        <div className="checkout-panel__buttons">
          {areBoxesFull ? (
            <>
              <button className="checkout-panel__button" onClick={handleAddAnotherBox}>Need more? Build another box
              </button>
              <a href="/checkout" className="checkout-panel__button checkout-panel__button--gray">Checkout</a>
            </>
          ) : (
            <>
              <button className="checkout-panel__button" onClick={handleFillYourBox}>PLEASE FILL YOUR BOX</button>
            </>
          )}
        </div>
      </div>

    </div>
  );
};

Cart.propTypes = {
  toggleCart: PT.func,
  isShown: PT.bool,
  boxes: PT.object,
  areBoxesFull: PT.bool,
  setStep: PT.func,
  getStep: PT.func,
  currentStep: PT.number,
};

const mapStateToProps = (state) => ({
  isShown: getCartIsShown(state),
  boxes: getBoxes(state),
  areBoxesFull: getAreBoxesFull(state),
  currentStep: getStep(state),
});

const mapDispatchToProps = {
  toggleCart,
  setStep
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
