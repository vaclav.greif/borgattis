import React from 'react';
import PT from "prop-types";

const ButtonPlus = (props) => {
  return props.buttonType === 'plus' ? (
    <button {...props}>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
        <path
          d="M256 0a256 256 0 101 513 256 256 0 00-1-513zm149 267c0 6-4 10-10 10H277v118c0 6-4 10-10 10h-22c-6 0-10-4-10-10V277H117c-6 0-10-4-10-10v-22c0-6 4-10 10-10h118V117c0-6 4-10 10-10h22c6 0 10 4 10 10v118h118c6 0 10 4 10 10v22z"/>
      </svg>
    </button>
  ) : (<button {...props}>Add to box</button>);
};

ButtonPlus.propTypes = {
  props: PT.object,
  buttonType: PT.string
};
export default ButtonPlus;
