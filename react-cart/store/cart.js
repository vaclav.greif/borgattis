import {createSlice, createSelector, createAsyncThunk} from '@reduxjs/toolkit'

const name = 'cart';

export const fetchAddBox = createAsyncThunk(
  'cart/fetchAddBox',
  async (args) => {
    const url = window.cartApp.restUrl;

    const response = await fetch(`${url}/box`, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        size: args.size,
        boxType: args.boxType
      })
    });

    const data = await response.json();

    return data;
  }
);

export const fetchChangeBoxSize = createAsyncThunk(
  'cart/fetchChangeBoxSize',
  async (args) => {
    const url = window.cartApp.restUrl;

    const response = await fetch(`${url}/box/${args.id}`, {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        size: args.size
      })
    });

    const data = await response.json();

    return data;
  }
);

export const fetchAddToCart = createAsyncThunk(
  'cart/fetchAddToCart',
  async (args) => {
    const url = window.cartApp.restUrl;

    const response = await fetch(`${url}/cart`, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        productId: args.productId,
        quantity: args.quantity,
        parentId: args.parentId,
      })
    });

    const data = await response.json();

    return data;
  }
);

export const fetchUpdateQuantity = createAsyncThunk(
  'cart/fetchUpdateQuantity',
  async (args) => {
    const url = window.cartApp.restUrl;

    const response = await fetch(`${url}/cart/${args.id}`, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        quantity: args.quantity,
      })
    });

    const data = await response.json();

    return data;
  }
);

export const fetchRemoveBox = createAsyncThunk(
  'cart/fetchRemoveBox',
  async (args) => {
    const url = window.cartApp.restUrl;

    const response = await fetch(`${url}/box/${args.id}`, {
      method: 'DELETE',
      headers: {'Content-Type': 'application/json'}
    });

    const data = await response.json();

    return data;
  }
);

export const fetchEmptyBox = createAsyncThunk(
  'cart/fetchEmptyBox',
  async (args) => {
    const url = window.cartApp.restUrl;

    const response = await fetch(`${url}/empty-box/${args.id}`, {
      method: 'DELETE',
      headers: {'Content-Type': 'application/json'}
    });

    const data = await response.json();

    return data;
  }
);

export const fetchCartContent = createAsyncThunk(
  'cart/fetchCartContent',
  async () => {
    const url = window.cartApp.restUrl;

    const response = await fetch(`${url}/cart/sorted`, {
      method: 'GET',
      headers: {'Content-Type': 'application/json'},
    });

    const data = await response.json();

    return data;
  }
);

// Slice

export const cartSlice = createSlice({
  name,
  initialState: null,
  reducers: {
    toggleCart(state, action) {
      state.isShown = action.payload;
    },
    setCurrentBox(state, action) {
      state.currentBox = action.payload;
    },
  },
  extraReducers: {
    [fetchCartContent.fulfilled]: (state, action) => {
      state.boxes = action.payload;
    },
    [fetchAddBox.fulfilled]: (state, action) => {
      state.boxes = action.payload.cart;
      state.currentBox = action.payload.id;
    },
    [fetchAddToCart.fulfilled]: (state, action) => {
      state.boxes = action.payload.cart;
      const currentBox = state.boxes.find((item) => item.box.id === state.currentBox);
      if (currentBox?.isFull) {
        state.isShown = true;
      }
    },
    [fetchUpdateQuantity.fulfilled]: (state, action) => {
      state.boxes = action.payload.cart;
      const currentBox = state.boxes.find((item) => item.box.id === state.currentBox);
      if (currentBox?.isFull) {
        state.isShown = true;
      }
    },
    [fetchEmptyBox.fulfilled]: (state, action) => {
      state.boxes = action.payload.cart;
    },
    [fetchRemoveBox.fulfilled]: (state, action) => {
      state.boxes = action.payload.cart;
      if (!state.boxes.length) {
        state.currentBox = null;
      } else {
        state.currentBox = state.boxes[0].box.id;
      }
    },
    [fetchChangeBoxSize.fulfilled]: (state, action) => {
      state.boxes = action.payload.cart;
    },
  }
});

export const toggleCart = cartSlice.actions.toggleCart;
export const setCurrentBox = cartSlice.actions.setCurrentBox;

// Selectors

const cartSelector = state => state[cartSlice.name];

export const getBoxes = createSelector(
  cartSelector,
  slice => slice.boxes,
);

export const getCartIsShown = createSelector(
  cartSelector,
  slice => {
      const queryString = window.location.search;
      const urlParams = new URLSearchParams(queryString);
      if (urlParams.get('show-cart') == 1) {
          return true;
      }
      return slice.isShown
  },
);

export const getCartQuantities = createSelector(
  cartSelector,
  (slice) => {
    const counts = {};
    slice.boxes.forEach((box) => {
      counts[box.box.id] = {};
      box.items.forEach((item) => {
        counts[box.box.id][item.productId] = {
          quantity: item.quantity,
          id: item.id,
        };
      });
    });
    return counts;
  },
);
export const getIsCurrentBoxFull = createSelector(
  cartSelector,
  (cartSlice) => {
    const currentBox = cartSlice.boxes.find((item) => item.box.id === cartSlice.currentBox);
    return currentBox?.isFull;
  },
);

export const getAreBoxesFull = createSelector(
  cartSelector,
  (cartSlice) => {
    let areFull = true;

    for (let box of cartSlice.boxes) {
      if (!box.isFull) {
        areFull = false;
        break;
      }
    }

    return areFull;
  },
);


export const getCurrentBoxType = createSelector(
  cartSelector,
  (cartSlice) => {
    const currentBox = cartSlice.boxes.find((item) => item.box.id === cartSlice.currentBox);
    return currentBox?.box.type;

  },
);

export const getCurrentBox = createSelector(
  cartSelector,
  (cartSlice) => {
    const box = cartSlice.boxes.find((item) => item.box.id === cartSlice.currentBox);
    return typeof box !== 'undefined' ? box : null;
  },
);

export const getCurrentBoxId = createSelector(
  cartSelector,
  slice => slice.currentBox,
);
