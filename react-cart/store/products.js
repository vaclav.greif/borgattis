import { createSlice, createSelector } from '@reduxjs/toolkit'

const name = 'products';

// Slice

export const productsSlice = createSlice({
  name,
  initialState: null,
  reducers: {},
});

// Selectors

export const productsSelector = state => state[productsSlice.name];

const paramsSelector = (state, params) => params;

export const getAvailableproducts = createSelector(
  productsSelector,
  slice => slice.all.ids.map(id => slice.all.entities[id]),
);

export const getBestsellers = createSelector(
  productsSelector,
  slice => slice.bestSellers.ids.map(id => slice.bestSellers.entities[id]),
);


export const getProduct = createSelector(
  productsSelector,
  paramsSelector,
  (slice, params) => slice.all.entities[params.id],
);
