import {configureStore, combineReducers} from '@reduxjs/toolkit';
import merge from 'deepmerge';
import {cartSlice} from './cart';
import {boxesSlice} from './boxes';
import {productsSlice} from './products';
import {categoriesSlice} from './categories';
import {wizardSlice, ZIP_UNKNOWN} from './wizard';

const loadState = () => {
  try {
    const serializedState = localStorage.getItem('borgattis-wizzard-state');

    if (serializedState === null) {
      return window.cartApp.state;
    }

    const overwriteMerge = (destinationArray, sourceArray) => sourceArray;

    const preloadedState = merge(
      JSON.parse(serializedState),
      window.cartApp.state,
      {arrayMerge: overwriteMerge}
    );

    if (!preloadedState.wizard) {
      preloadedState.wizard = {};
    }

    const hashMatch = /#step(?<step>\d+)/.exec(window.location.hash);
    if (hashMatch && hashMatch.groups.step) {
      preloadedState.wizard.step = parseInt(hashMatch.groups.step, 10);
    } else if (!preloadedState.wizard.step) {
      preloadedState.wizard.step = 1;
      window.location.hash = '#step' + preloadedState.wizard.step;
    }

    if (!preloadedState.wizard.zip) {
      preloadedState.wizard.zip = '';
      preloadedState.wizard.zipValid = ZIP_UNKNOWN;
    }

    return preloadedState;
  } catch (err) {
    return window.cartApp.state;
  }
};

const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('borgattis-wizzard-state', serializedState);
  } catch {
    //
  }
};

export const resetState = () => {
  saveState(window.cartApp.state);
};

const preloadedState = loadState();

const rootReducer = combineReducers({
  [cartSlice.name]: cartSlice.reducer,
  [boxesSlice.name]: boxesSlice.reducer,
  [productsSlice.name]: productsSlice.reducer,
  [categoriesSlice.name]: categoriesSlice.reducer,
  [wizardSlice.name]: wizardSlice.reducer,
});

const store = configureStore({
  reducer: rootReducer,
  preloadedState,
});

store.subscribe(() => {
  saveState({
    cart: store.getState().cart,
    wizard: store.getState().wizard,
  });
});

export default store;
