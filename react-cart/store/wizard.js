import { createSlice, createSelector, createAsyncThunk } from '@reduxjs/toolkit';

const name = 'wizard';

export const ZIP_VALID = 'ZIP_VALID';
export const ZIP_INVALID = 'ZIP_INVALID';
export const ZIP_UNKNOWN = 'ZIP_UNKNOWN';

// Actions

export const setZip = createAsyncThunk(
  'cart/setZip',
  async (postcode) => {
    const url = window.cartApp.restUrl;

    const response = await fetch(`${url}/postcode`, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({postcode}),
    });

    const data = await response.json();

    return data;
  }
);

// Slice

export const wizardSlice = createSlice({
  name,
  initialState: null,
  reducers: {
    setStep(state, action) {
      state.step = action.payload;
      window.location.hash = '#step' + action.payload;
    },
  },
  extraReducers: {
    [setZip.fulfilled]: (state, action) => {
      state.zipValid = action.payload.valid ? ZIP_VALID : ZIP_INVALID;
      state.zip = action.payload.postcode;
    },
  },
});

// Actions

export const setStep = wizardSlice.actions.setStep;

// Selectors

export const wizardSelector = state => state[name] || {};

export const getStep = createSelector(
  wizardSelector,
  slice => slice.step,
);



export const getZip = createSelector(
  wizardSelector,
  slice => ({value: slice.zip, state: slice.zipValid}),
);
