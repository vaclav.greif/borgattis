import {createSlice} from '@reduxjs/toolkit'


const name = 'boxes';


// Slice

export const boxesSlice = createSlice({
  name,
  initialState: null,
  reducers: {},
});

// Selectors

export const getAvailableBoxes = state => state[boxesSlice.name];
