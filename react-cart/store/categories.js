import { createSlice, createSelector } from '@reduxjs/toolkit'

const name = 'categories';

// Slice

export const categoriesSlice = createSlice({
  name,
  initialState: null,
  reducers: {},
});

// Selectors

export const categoriesSelector = state => state[categoriesSlice.name];

export const getAvailableCategories = createSelector(
  categoriesSelector,
  slice => slice.ids.map(id => slice.entities[id]),
);
