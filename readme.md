# Borgattis.com feature plugin

## Prerequisities

- Node.js > v12.12.0
- Yarn 1.X
- Composer

## Development

1. Go to the plugin folder `wp-content/plugins/borgattis`
2. Install Composer dependencies `composer install`
3. Install Node dependencies `yarn install`
4. Start development with `yarn start`
5. Build assets with `yarn build`
