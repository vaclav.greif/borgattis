/* eslint-disable no-undef */
jQuery(document).ready(function ($) {
  var borgattisCheckout = function () {
    return {
      init: function () {
        $('.checkout__btn-next--shipping').on('click', this.validateCustomer);
        $('.checkout__btn-next--payment').on('click', this.showPayment);
        $('.checkout__steps li').on('click', this.switchStep);
      },
      showShipping: function () {
        $('.checkout__step--customer').hide();
        $('.checkout__step--shipping').show();
        $('.checkout__steps li').removeClass('active');
        $('.checkout__steps li[data-step="shipping"]').addClass('active');
        $("html, body").animate({scrollTop: $('.checkout__steps').offset().top - 200}, 500);
      },
      showPayment: function () {
        $('.checkout__step--shipping').hide();
        $('.checkout__step--payment').show();
        $('.checkout__steps li').removeClass('active');
        $('.checkout__steps li[data-step="payment"]').addClass('active');
        $("html, body").animate({scrollTop: $('.checkout__steps').offset().top - 200}, 500);
      },
      switchStep: function () {
        let step = $(this).data('step');

        if ((step == 'shipping' || step == 'payment') && !$('#customer_details').hasClass('validated')) {
          return false;
        }
        $('.checkout__steps li').removeClass('active');
        $(this).addClass('active');
        $('.checkout__step').hide();
        $('.checkout__step--' + step).show();
        $("html, body").animate({scrollTop: $('.checkout__steps').offset().top - 200}, 500);
      },
      validateCustomer: function () {
        $.ajax({
          url: borgattisCheckoutVars.restUrl + '/checkout/validate/customer',
          method: 'POST',
          data: $('.woocommerce-checkout').serialize(),
          beforeSend: function () {
            $('.checkout__response').html('').removeClass('checkout__response--error');
            $('#customer_details').removeClass('validated');
          },
          success: function (response) {
            if (!response.errors.length) {
              $('.checkout__response').html('');
              $('#customer_details').addClass('validated');
              $('.invalid').removeClass('invalid');
              borgattisCheckout().showShipping();
            } else {
              response.errors.forEach(item => {
                $('input[name="' + item.field + '"').addClass('invalid');
              });
              $('.checkout__response').html(response.formatted_errors).addClass('checkout__response--error');
              $("html, body").animate({scrollTop: $('.checkout__steps').offset().top - 200}, 500);
            }
          }
        }, 'json');

      }
    }
  };
  borgattisCheckout().init();
});
