jQuery(document).ready(function ($) {
  if (!$('.mtd').length) {
    return false;
  }
  var product_id = 0;
  var variation_id = 0;
  var variations_data = {};

  // $('.single_add_to_cart_button').on('click', function (e) {
  //   if ($(this).hasClass('disabled')) {
  //     return;
  //   }
  //   e.preventDefault();
  //
  //   var $form = $(this).closest('form')
  //   product_id = $form.find('input[name="product_id"]').val();
  //   if (!product_id) {
  //     product_id = $form.find('input[name="add-to-cart"]').val();
  //   }
  //   variation_id = $form.find('input[name="variation_id"]').val();
  //
  //   var variationsDataForm = $form.data('product_variations');
  //   if (variationsDataForm) {
  //     variations_data = {};
  //     var variations = $form.data('product_variations')[0].attributes;
  //     Object.keys(variations).forEach(function (key, index) {
  //       variations_data[key] = $('[name="' + key + '"]').val();
  //     });
  //   }
  //   $('.mtd__products').hide();
  //   $('.mtd__form').show();
  //   $("html, body").animate({scrollTop: 0});
  // });

  $('.mtd__form').on('submit', function (e) {
    e.preventDefault();
    var data = $(this).serializeArray(); // convert form to array
    data.push({name: 'product_id', value: product_id});
    data.push({name: 'variation_id', value: variation_id});
    Object.keys(variations_data).forEach(function (key, index) {
      data.push({name: 'variation_data[' + key + ']', value: variations_data[key]});
    });

    console.log(data);
    console.log($.param(data));
    $.ajax({
      type: 'POST',
      url: mtd.restUrl + '/cart/mtd',
      data: $.param(data),
      success: function (response) {
        console.log('aaaa');
        window.location.href = mtd.checkoutUrl;
      }
    }, 'json');
  })
  $('.mtd__gift-header').on('click', function () {
    $('.mtd__gift-content').toggle();
  })
});

