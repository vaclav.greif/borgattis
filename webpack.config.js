const defaultConfig = require('@wordpress/scripts/config/webpack.config');

module.exports = {
  ...defaultConfig,
  entry: {
    cartApp: './react-cart/entry.jsx',
    'checkout-navigation': './react-cart/checkout-navigation.jsx',
    'plugin': './react-cart/assets/plugin.scss',
  },
  resolve: {
    ...defaultConfig.resolve,
    extensions: ['.js', '.json', '.jsx'],
  }
};
