<?php

/*
 * Plugin Name: Borgattis
 * Version: 3.1.4
 * Text Domain:     borgattis
 * Domain Path:     /languages
*/

use ComposePress\Dice\Dice;


/**
 * Singleton instance function. We will not use a global at all as that defeats the purpose of a singleton and is a bad design overall
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @return Borgattis\Plugin
 */
function borgattis()
{
  return borgattis_container()->create('\Borgattis\Plugin');
}

/**
 * This container singleton enables you to setup unit testing by passing an environment file to map classes in Dice
 *
 * @param string $env
 *
 * @return \ComposePress\Dice\Dice
 */
function borgattis_container($env = 'prod')
{
  static $container;
  if (empty($container)) {
    $container = new Dice();
    include __DIR__ . "/config-{$env}.php";
  }

  return $container;
}

/**
 * Init function shortcut
 */
function borgattis_init()
{
  borgattis()->init();
}

/**
 * Activate function shortcut
 */
function borgattis_activate($network_wide)
{
  register_uninstall_hook(__FILE__, 'borgattis_uninstall');
  borgattis()->init();
  borgattis()->activate($network_wide);
}

/**
 * Deactivate function shortcut
 */
function borgattis_deactivate($network_wide)
{
  borgattis()->deactivate($network_wide);
}

/**
 * Uninstall function shortcut
 */
function borgattis_uninstall()
{
  borgattis()->uninstall();
}

/**
 * Error for older php
 */
function borgattis_php_upgrade_notice()
{
  $info = get_plugin_data(__FILE__);
  _e(
    sprintf(
      '
	<div class="error notice">
		<p>Opps! %s requires a minimum PHP version of 5.4.0. Your current version is: %s. Please contact your host to upgrade.</p>
	</div>',
      $info['Name'],
      PHP_VERSION
    )
  );
}

/**
 * Error if vendors autoload is missing
 */
function borgattis_php_vendor_missing()
{
  $info = get_plugin_data(__FILE__);
  _e(
    sprintf(
      '
	<div class="error notice">
		<p>Opps! %s is corrupted it seems, please re-install the plugin.</p>
	</div>',
      $info['Name']
    )
  );
}

/*
 * We want to use a fairly modern php version, feel free to increase the minimum requirement
 */
if (version_compare(PHP_VERSION, '5.4.0') < 0) {
  add_action('admin_notices', 'borgattis_php_upgrade_notice');
} else {
  if (file_exists(__DIR__ . '/vendor/autoload.php')) {
    include_once __DIR__ . '/vendor/autoload.php';
    if(! defined( 'WP_SANDBOX_SCRAPING' )) {
      borgattis_init();
      register_activation_hook(__FILE__, 'borgattis_activate');
      register_deactivation_hook(__FILE__, 'borgattis_deactivate');
    }
  } else {
    add_action('admin_notices', 'borgattis_php_vendor_missing');
  }
}
